package com.nicta.metagraph.umls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nicta.metagraph.NotFoundException;
import com.nicta.metagraph.umls.Metathesaurus.Concept;
import com.nicta.metagraph.umls.Metathesaurus.Entry;
import com.nicta.metagraph.umls.Metathesaurus.EntryAuxField;
import com.nicta.metagraph.umls.Metathesaurus.SemType;

@RunWith(JUnit4.class)
public class MetathesaurusTest {
	private static final Logger LOG = LoggerFactory.getLogger(MetathesaurusTest.class);
	
	static ImportMetathesaurusTest importer;
	private static Metathesaurus metathes;
	
	static boolean[] eagerVals = new boolean[] { false, true };
	
	static List<List<EntryAuxField>> eagerFieldSets = new ArrayList<List<EntryAuxField>>();
	
	static {
		List<EntryAuxField> list1 = new ArrayList<EntryAuxField>();
		list1.add(EntryAuxField.CUI);
		List<EntryAuxField> list2 = new ArrayList<EntryAuxField>();
		list2.add(EntryAuxField.PREF_LABEL);
		List<EntryAuxField> list3 = new ArrayList<EntryAuxField>();
		list3.add(EntryAuxField.PREF_LABEL);
		list3.add(EntryAuxField.CUI);
		eagerFieldSets.add(list1);
		eagerFieldSets.add(list2);
		eagerFieldSets.add(list3);
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
		importer = new ImportMetathesaurusTest();
		metathes = new Metathesaurus(importer.settings);
		doImport();
	}
	

	@Test
	public void basicConceptRetrieve() throws Exception {
		Concept margPizCon = metathes.findConceptByCui("C0000005");
		Assert.assertEquals("C0000005", margPizCon.nativeId());
		List<Entry> entries = margPizCon.entries();
		Assert.assertEquals(1, entries.size());
		Entry margPizEnt = entries.get(0);
		Assert.assertEquals("FOOD/D000005", margPizEnt.prefixedNativeId());
		Assert.assertEquals("D000005", margPizEnt.bareNativeId());
		Assert.assertEquals("Margherita Pizza", margPizEnt.prefLabel());
	}
	
	@Test
	public void basicEntryRetrieve() throws Exception {
		Entry hawPizEnt = metathes.findEntryByNativeId("FOOD", "D000004");
		Entry hawPizEnt2 = metathes.findEntryByPrefixedId("FOOD/D000004");
		Entry margPizEnt = metathes.findEntryByNativeId("FOOD", "D000005");
		Assert.assertTrue(hawPizEnt.equals(hawPizEnt2));
		Assert.assertFalse(hawPizEnt.equals(margPizEnt));
		Assert.assertEquals("Hawaiian Pizza", hawPizEnt.prefLabel());
		SemType prepFoodType = metathes.findSemTypeById("T005");
		Assert.assertTrue(hawPizEnt.semTypes().contains(prepFoodType));
		SemType orgSubstType = metathes.findSemTypeById("T002");
		Assert.assertTrue(hawPizEnt.semTypes().contains(orgSubstType));
	}
	
	@Test
	public void parentsAndChildren() throws Exception {
		Entry pizzaEnt = metathes.findEntryByNativeId("FOOD", "D000003");
		Entry foodEnt = metathes.findEntryByNativeId("FOOD", "D000002");
		Entry margEnt = metathes.findEntryByNativeId("FOOD", "D000005");
		Entry pineappleEnt = metathes.findEntryByNativeId("FOOD", "D000006");
		Entry consumableEnt = metathes.findEntryByNativeId("FOOD", "D000001");
		for (boolean eager :  eagerVals) {
			Assert.assertTrue(pizzaEnt.parents(eager).contains(foodEnt));
			Assert.assertFalse(foodEnt.parents(eager).contains(pizzaEnt)); // wrong way
			Assert.assertTrue(pizzaEnt.children(eager).contains(margEnt));
			Assert.assertFalse(margEnt.children(eager).contains(pizzaEnt)); //wrong way
			Assert.assertFalse(foodEnt.children(eager).contains(margEnt)); // indirect
			Assert.assertFalse(pizzaEnt.children(eager).contains(pineappleEnt)); //invalid
			Assert.assertTrue(margEnt.children(eager).size() == 0);
			Assert.assertTrue(consumableEnt.parents(eager).size() == 0);
			Assert.assertTrue(getPrefLabels(pizzaEnt.children(eager)).contains("Hawaiian Pizza"));
		}
		for (List<EntryAuxField> fieldSet : eagerFieldSets ) {
			Assert.assertTrue(pizzaEnt.ancestors(fieldSet, true).contains(foodEnt));
			Assert.assertFalse(foodEnt.ancestors(fieldSet, true).contains(pizzaEnt)); // wrong way
			Assert.assertTrue(pizzaEnt.descendants(fieldSet, true).contains(margEnt));
			Assert.assertFalse(margEnt.descendants(fieldSet, true).contains(pizzaEnt)); //wrong way
			Assert.assertFalse(foodEnt.descendants(fieldSet, true).contains(margEnt)); // indirect
			Assert.assertFalse(pizzaEnt.descendants(fieldSet, true).contains(pineappleEnt)); //invalid
			Assert.assertTrue(margEnt.descendants(fieldSet, true).size() == 0);
			Assert.assertTrue(consumableEnt.ancestors(fieldSet, true).size() == 0);
			Assert.assertTrue(getPrefLabels(pizzaEnt.descendants(fieldSet, true)).contains("Hawaiian Pizza"));
		}
	}
	
	@Test
	public void ancestorsAndDescendants() throws Exception {
		Entry pizzaEnt = metathes.findEntryByNativeId("FOOD", "D000003");
		Entry foodEnt = metathes.findEntryByNativeId("FOOD", "D000002");
		Entry consumableEnt = metathes.findEntryByNativeId("FOOD", "D000001");
		Entry margEnt = metathes.findEntryByNativeId("FOOD", "D000005");
		Entry pineappleEnt = metathes.findEntryByNativeId("FOOD", "D000006");
		for (boolean eager : eagerVals) {
			Assert.assertTrue(pizzaEnt.ancestors(eager).contains(foodEnt));
			Assert.assertTrue(pizzaEnt.ancestors(eager).contains(consumableEnt));
			Assert.assertTrue(margEnt.ancestors(eager).contains(consumableEnt));
			Assert.assertTrue(pizzaEnt.descendants(eager).contains(margEnt));
			Assert.assertTrue(foodEnt.descendants(eager).contains(margEnt));
			Assert.assertTrue(consumableEnt.descendants(eager).contains(margEnt));
			Assert.assertFalse(pizzaEnt.descendants(eager).contains(pineappleEnt)); // invalid
			Assert.assertFalse(pineappleEnt.descendants(eager).contains(pizzaEnt)); // invalid
			Assert.assertFalse(pizzaEnt.descendants(eager).contains(consumableEnt)); // wrong way
			Assert.assertFalse(foodEnt.ancestors(eager).contains(pizzaEnt)); // wrong way
			Assert.assertTrue(margEnt.descendants(eager).size() == 0);
			Assert.assertTrue(consumableEnt.ancestors(eager).size() == 0);
			Assert.assertTrue(getPrefLabels(pizzaEnt.descendants(eager)).contains("Hawaiian Pizza"));
			Assert.assertTrue(getPrefLabels(pineappleEnt.ancestors(eager)).contains("Consumable"));
		}
		for (List<EntryAuxField> fieldSet : eagerFieldSets ) {
			Assert.assertTrue(pizzaEnt.ancestors(fieldSet).contains(foodEnt));
			Assert.assertTrue(pizzaEnt.ancestors(fieldSet).contains(consumableEnt));
			Assert.assertTrue(margEnt.ancestors(fieldSet).contains(consumableEnt));
			Assert.assertTrue(pizzaEnt.descendants(fieldSet).contains(margEnt));
			Assert.assertTrue(foodEnt.descendants(fieldSet).contains(margEnt));
			Assert.assertTrue(consumableEnt.descendants(fieldSet).contains(margEnt));
			Assert.assertFalse(pizzaEnt.descendants(fieldSet).contains(pineappleEnt)); // invalid
			Assert.assertFalse(pineappleEnt.descendants(fieldSet).contains(pizzaEnt)); // invalid
			Assert.assertFalse(pizzaEnt.descendants(fieldSet).contains(consumableEnt)); // wrong way
			Assert.assertFalse(foodEnt.ancestors(fieldSet).contains(pizzaEnt)); // wrong way
			Assert.assertTrue(margEnt.descendants(fieldSet).size() == 0);
			Assert.assertTrue(consumableEnt.ancestors(fieldSet).size() == 0);
			Assert.assertTrue(getPrefLabels(pizzaEnt.descendants(fieldSet)).contains("Hawaiian Pizza"));
			Assert.assertTrue(getPrefLabels(pineappleEnt.ancestors(fieldSet)).contains("Consumable"));
		}
	}
	
	private List<String> getPrefLabels(Collection<Entry> ents) {
		List<String> prefLabels = new ArrayList<String>();
		for (Entry ent: ents) 
			prefLabels.add(ent.prefLabel());
		return prefLabels;
	}
		
	
	@Test(expected = NotFoundException.class)
	public void entryNotFound() {
		metathes.findEntryByNativeId("FOOD", "NONEXIST01");
	}
	
	@Test(expected = NotFoundException.class)
	public void ontNotFound() {
		metathes.findEntryByNativeId("NONEXIST", "D000005");
	}
	
	@Test
	public void sourceSpecRels() {
		Entry hawaiianEnt = metathes.findEntryByNativeId("FOOD", "D000004");
		Entry pineappleEnt = metathes.findEntryByNativeId("FOOD", "D000006");
		Entry cheeseEnt = metathes.findEntryByNativeId("FOOD", "D000007");
		Entry pizzaEnt = metathes.findEntryByNativeId("FOOD", "D000003");
		Entry margEnt =  metathes.findEntryByNativeId("FOOD", "D000005");
		Assert.assertTrue(hawaiianEnt.sourceSpecificRels().get("has_topping").contains(pineappleEnt));
		Assert.assertFalse(margEnt.sourceSpecificRels().get("has_topping").contains(pineappleEnt));
		Assert.assertTrue(hawaiianEnt.sourceSpecificRels().get("has_topping").contains(cheeseEnt));
		Assert.assertTrue(margEnt.sourceSpecificRels().get("has_topping").contains(cheeseEnt));
		Assert.assertFalse(hawaiianEnt.sourceSpecificRels().get("has_topping").contains(hawaiianEnt));
		Assert.assertFalse(hawaiianEnt.sourceSpecificRels().get("has_topping").contains(pizzaEnt));
		Assert.assertNull(cheeseEnt.sourceSpecificRels().get("has_topping")); 
		Assert.assertNull(pineappleEnt.sourceSpecificRels().get("has_topping")); 
	}


	@Test
	public void showEntries() throws Exception {
		RepositoryConnection conn = metathes.conn;
		LOG.debug("Can now find {} contexts", conn.size(metathes.graphUri));
		TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL,
				"SELECT ?s ?p ?o WHERE { ?s ?p ?o } LIMIT 10");
		query.setDataset(metathes.dataset);
		TupleQueryResult result = query.evaluate();
		LOG.debug("Found {} results in graph", (result.hasNext() ? " > 0" : "0"));
		while (result.hasNext()) {
			BindingSet bs = result.next();
			LOG.trace("Sample triple: {} {} {} ", bs.getBinding("s"), bs.getBinding("p"), bs.getBinding("o"));
		}
	}

	private static void doImport() throws Exception {
		importer.importTurtleFiles();
		System.out.println(String.format("Finished import; can now find %s contexts", 
				metathes.conn.size(metathes.graphUri)));
	}

}
