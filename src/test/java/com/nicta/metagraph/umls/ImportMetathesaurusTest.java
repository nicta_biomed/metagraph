package com.nicta.metagraph.umls;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openrdf.OpenRDFException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;

import com.nicta.metagraph.MetagraphConfigException;
import com.nicta.metagraph.Settings;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigParseOptions;
import com.typesafe.config.ConfigResolveOptions;

@RunWith(JUnit4.class)
public class ImportMetathesaurusTest {
	// see src/test/resources/test.conf for config
	Config testConf;
	Settings settings;

	public ImportMetathesaurusTest() throws MetagraphConfigException, SQLException {
		init();
	}
	
	private void init() {
		ConfigParseOptions opts  = ConfigParseOptions.defaults().setAllowMissing(false);
		testConf = ConfigFactory.load("test", opts, ConfigResolveOptions.defaults());
		settings = new Settings(testConf);
	}

	@Test
	public void generateTurtleFiles() throws IOException,
			SQLException, MetagraphConfigException, OpenRDFException {
		recreateTargetDir();
		ImportMetathesaurus.run(settings, true, false, true, false);
	}

	@Test(expected = MetagraphConfigException.class)
	public void unknownOnt() {
		// throws an exception as the ontology key doesn't exist
		recreateTargetDir();
		ImportMetathesaurus.generate(settings.umls().rrfPath(), settings.umls()
				.generatedTurtlePath(), Arrays.asList("BAH"));
	}

	@Test
	public void importTurtleFiles() throws IOException,
			SQLException, MetagraphConfigException, OpenRDFException {
		recreateTargetDir();
		ImportMetathesaurus.run(testConf, true, false, false);
	}
	
	public File targetDir() {
		return settings.umls().generatedTurtlePath();
	}

	private void recreateTargetDir() {
		if (targetDir().exists())
			for (File f : targetDir().listFiles())
				f.delete();
		else
			targetDir().mkdirs();
	}
}
