package com.nicta.metagraph;

public class MissingUMLSDataException extends MetagraphConfigException {

	public MissingUMLSDataException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public MissingUMLSDataException(Throwable e) {
		super(e);
		// TODO Auto-generated constructor stub
	}

}
