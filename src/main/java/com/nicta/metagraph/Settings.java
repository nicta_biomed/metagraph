package com.nicta.metagraph;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nicta.metagraph.sesame.InMemoryRepoFactory;
import com.nicta.metagraph.sesame.RepoFactory;
import com.nicta.metagraph.sesame.VirtuosoRepoFactory;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigResolveOptions;

/** Wrapper around typesafe-config library for configuring Metagraph
 * 
 * This will search for HOCON-formatted config files according the search algorithm
 * of typesafe-config {@link Config} loader. 
 * 
 * @author amack
 *
 */
public class Settings {
	private static final Logger LOG = LoggerFactory.getLogger(Settings.class);

	public static final String DEFAULT_PREFIX = "metagraph";

	private static Config getDefault() {
		ConfigResolveOptions resolveOptions = ConfigResolveOptions.defaults()
				.setUseSystemEnvironment(true);
		return ConfigFactory.load().resolve(resolveOptions);
	}

	private final Config conf;
	private final Umls _umls;
	private final Sesame _sesame;

	/** Construct a new instance from the default config file found
	 * according to the typesafe-config library rules.
	 */
	public Settings() {
		this(getDefault(), true);
	}

	/** Create a new Settings instance from the supplied config object.
	 * 
	 * Expects config items to be under "metagraph.sesame" and "metagraph.umls"
	 */
	public Settings(Config conf) {
		this(conf, true);
	}

	/** Create a new Settings instance from the supplied config object
	 * 
	 * 
	 * @param conf a pre-parsed config object
	 * @param hasPrefix if `true`, assumes that the required config items ("sesame", "umls")
	 *   are nested under a top-level prefix `metagraph`. 
	 */
	public Settings(Config conf, boolean hasPrefix) {
		this.conf = hasPrefix ? conf.getConfig(DEFAULT_PREFIX) : conf;
		_sesame = new Sesame();
		_umls = new Umls();
	}
	
	public Sesame sesame() {
		return _sesame;
	}

	public Umls umls() {
		return _umls;
	}

	public class Sesame {
		public String backend() {
			return conf.getString("sesame.backend");
		}

		public int virtuosoPort() {
			return conf.getInt("sesame.virtuoso.port");
		}

		public String virtuosoHost() {
			return conf.getString("sesame.virtuoso.host");
		}

		public String virtuosoUsername() {
			return conf.getString("sesame.virtuoso.username");
		}

		public String virtuosoPassword() {
			return conf.getString("sesame.virtuoso.password");
		}

		public File virtuosoImportDir() {
			return new File(conf.getString("sesame.virtuoso.import-directory"));
		}

		public VirtuosoRepoFactory virtuosoRepoFactory() throws SQLException, MetagraphConfigException {
			if (!backend().equals("virtuoso"))
				LOG.warn("Virtuso requested, but the configured backend is not Virtuoso");
			return new VirtuosoRepoFactory(virtuosoHost(), virtuosoPort(), virtuosoUsername(),
					virtuosoPassword());
		}

		public RepoFactory repoFactory() throws MetagraphConfigException, SQLException, RepositoryException {
			if (backend().equals("virtuoso"))
				return virtuosoRepoFactory();
			else if (backend().equals("inmem"))
				return new InMemoryRepoFactory();
			else
				throw new MetagraphConfigException("Unknown backend " + backend());
		}


		public String graphUri() {
			return conf.getString("sesame.graph-uri");
		}
	}

	public class Umls {
		public File generatedTurtlePath() {
			return new File(conf.getString("umls.generated-turtle-path"));
		}

		public File rrfPath() {
			return new File(conf.getString("umls.rrf-path"));
		}

		public List<String> ontologies() {
			return conf.getStringList("umls.ontologies");
		}

	}

}
