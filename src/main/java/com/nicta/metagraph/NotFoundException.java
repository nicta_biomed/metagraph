package com.nicta.metagraph;

public class NotFoundException extends RuntimeException {
	/** An expected entry was not found
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	public NotFoundException(String string) {
		super(string);
	}

}
