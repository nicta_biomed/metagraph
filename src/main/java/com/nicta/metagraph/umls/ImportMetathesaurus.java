package com.nicta.metagraph.umls;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.openrdf.OpenRDFException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import virtuoso.jdbc4.VirtuosoException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.nicta.metagraph.Settings;
import com.nicta.metagraph.MetagraphConfigException;
import com.nicta.metagraph.MissingUMLSDataException;
import com.nicta.metagraph.umls.RDFConverter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;

public class ImportMetathesaurus {
	private static final Logger LOG = LoggerFactory.getLogger(ImportMetathesaurus.class);

	private static final String USAGE = "Import UMLS Metathesaurus into a Sesame triple store; \n";
	private static final String CONF_FILES_USAGE = "Metagraph uses configuration variables read by the Typesafe Config library, which "
			+ "looks for config files in the classpath according the rules at "
			+ "https://github.com/typesafehub/config . Essentially you should put your "
			+ "configuration in a file called 'application.conf' in the classpath or a file included "
			+ "by an application.conf, or redefine relevant config using '-Dconf.var=value';\n"
			+ "Defaults, if not overridden elsewhere, come from the file 'reference.conf' which is "
			+ "included with this library\n";
	private static final String CONF_VARS_USAGE = "Relevant configuration variables you will probably want to override: \n"
			+ "'metagraph.umls.rrf-path': where the UMLS RRF files are stored \n"
			+ "'metagraph.umls.ontologies': which ontologies to include \n"
			+ "'metagraph.sesame.virtuoso.{username,password}': Credentials for Virtuoso \n"
			+ "\nOther variables you might want to play with:\n"
			+ "'metagraph.sesame.virtuoso.{host,port}' - where to find the Virtuoso server \n"
			+ "'metagraph.sesame.graph-uri-base' - the URI prefix for the ontology graph - \n"
			+ "     Override this to have multiple Metagraph instances with different ontologies\n"
			+ "     on a single Virtuoso server \n"
			+ "'metagraph.umls.generated-turtle-path': Where the .TTL files are written to\n"
			+ "'metagraph.sesame.virtuoso.import-directory': A location configured in the Virtuoso \n"
			+ "     server config for allowing disk-based imports (if not set, in-memory imports \n"
			+ "     are used which is probably what you want - i.e. you probably *don't* need \n"
			+ "     to set this; in addition it seems to trigger bugs in some Virtuoso versions) \n";

	/** generate .TTL files from RRFs */
	public static void generate(File sourcePath, File targetTurtlePath,
			Iterable<String> ontologyKeys, boolean cache) {
		RDFConverter.writeTriples(sourcePath, targetTurtlePath, ontologyKeys, "2012AB", true,
				false, false, cache);
	}

	public static void generate(File sourcePath, File targetTurtlePath,
			Iterable<String> ontologyKeys) {
		generate(sourcePath, targetTurtlePath, ontologyKeys, false);
	}

	protected static class CLParams {

		@Parameter(names = { "-f", "--force-regen" }, required = false, description = "Regenerate TTLs from RRFs even if they already exist")
		private boolean forceRegen = false;

		@Parameter(names = { "-s", "--suppress-import" }, required = false, description = "Do not actually import into RDF repository; generate only, if other flags would make this happen")
		private boolean suppressImport = false;

		@Parameter(names = { "-k", "--keep-existing" }, description = "Keep existing triplestore entries")
		private boolean keepExisting = false;

		@Parameter(names = { "--cache" }, required = false, description = "(EXPERIMENTAL; PROBABLY BROKEN) Cache data read from RRF instead of reading through multiple times where there are multiple sources in use -- can greatly increase time for many sources, but will decrease memory use by a lot")
		private boolean cache = false;

		// @Parameter(names = { "-v", "--virtuoso" }, required = true, converter
		// = FileConverter.class,
		// description =
		// "Path to a directory from which virtuoso is allowed to import" +
		// " (overriding config value \"metagraph.sesame.virtuosoImportDir\"")
		// private File virtuosoImportDir;
		//
		// @Parameter(names = { "-t", "--turtle-path" }, required = false,
		// converter = FileConverter.class,
		// description =
		// "Root of cached generated .TTL files; if this directory is non-empty, it"
		// +
		// " won't be regenerated unless --force-regen is set; ")
		// private File turtlePath;
		//
		// @Parameter(names = { "-u", "--umls-rrf-path" }, required = true,
		// converter = FileConverter.class, description =
		// "Path to metamap Rich Release Format")
		// private File rrfPath;

		@Parameter(names = { "-h", "--help" }, help = true)
		private boolean help;
	}

	public static void run() throws IOException, SQLException, MetagraphConfigException,
			OpenRDFException {
		run(new Settings(), false, false, false, true);
	}

	public static void run(boolean forceRegen, boolean keepExisting, boolean suppressImport)
			throws IOException, SQLException, MetagraphConfigException, OpenRDFException {
		run(new Settings(), forceRegen, keepExisting, suppressImport, false);
	}

	/**
	 * Import stored metathesaurus data
	 * 
	 * @param conf
	 * @param forceRegen
	 * @param keepExisting
	 * @param suppressImport
	 * @param forceInsertPrototypes
	 * @throws IOException
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 * @throws OpenRDFException
	 */
	public static void run(Settings conf, boolean forceRegen, boolean keepExisting,
			boolean suppressImport, boolean cache) throws IOException, SQLException,
			MetagraphConfigException, OpenRDFException {
		File turtlePath = conf.umls().generatedTurtlePath();
		if (turtlePath.exists() && !turtlePath.isDirectory())
			throw new MetagraphConfigException(turtlePath + " exists and is not a directory");
		if (!turtlePath.exists()) {
			boolean created = turtlePath.mkdir();
			if (!created)
				throw new MetagraphConfigException("Could not create " + turtlePath
						+ "; verify that the parent directory exists and is writable");
		}
		if (forceRegen || turtlePath.listFiles().length == 0)
			generate(conf.umls().rrfPath(), turtlePath, conf.umls().ontologies(), cache);
		Metathesaurus metathes = new Metathesaurus(conf);
		if (!keepExisting && !suppressImport) {
			LOG.info("Clearing existing entries");
			metathes.clear();
		}
		if (!suppressImport) {
			File virtImportDir;
			try {
				virtImportDir = conf.sesame().virtuosoImportDir();
			} catch (ConfigException.Missing e) {
				virtImportDir = null;
			}
			metathes.importTurtleFromDir(turtlePath, virtImportDir);
		}
	}

	public static void run(Config conf, boolean forceRegen, boolean keepExisting,
			boolean suppressImport) throws IOException, SQLException, MetagraphConfigException,
			OpenRDFException {
		run(new Settings(conf), forceRegen, keepExisting, suppressImport, false);
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws RepositoryException
	 * @throws RDFParseException
	 * @throws MetagraphConfigException
	 * @throws SQLException
	 */
	public static void main(String[] args) throws Exception {
		CLParams params = new CLParams();
		JCommander jcom = null;
		try {
			jcom = new JCommander(params, args);
		} catch (ParameterException pe) {
			System.err.println(pe);
			usage(new JCommander(params, new String[] {}), false);
			System.exit(1);
		}
		if (params.help) {
			usage(jcom, true);
			return;
		}
		Settings conf = new Settings();
		try {
			run(conf, params.forceRegen, params.keepExisting, params.suppressImport, params.cache);
		} catch (MissingUMLSDataException e) {
			System.err.println(e + "; make sure you have correctly set \"metagraph.umls.rrf-path\"; use '-h' for help");
			System.exit(1);
		}
	}

	public static void usage(JCommander jcom, boolean verbose) {
		jcom.setProgramName(ImportMetathesaurus.class.getName());
		System.err.println(USAGE);
		jcom.usage();
		if (verbose) {
			System.err.println(CONF_FILES_USAGE);
			System.err.println(CONF_VARS_USAGE);
		}
	}
}
