package com.nicta.metagraph.umls;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.collections15.MultiMap;
import org.apache.commons.collections15.multimap.MultiHashMap;
import org.apache.commons.io.FileUtils;
import org.openrdf.OpenRDFException;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.query.Update;
import org.openrdf.query.UpdateExecutionException;
import org.openrdf.query.impl.DatasetImpl;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nicta.metagraph.MetagraphConfigException;
import com.nicta.metagraph.NotFoundException;
import com.nicta.metagraph.Settings;
import com.nicta.metagraph.Util;
import com.nicta.metagraph.sesame.InvalidURIException;
import com.nicta.metagraph.sesame.RepoFactory;
import com.nicta.metagraph.sesame.VirtuosoRepoFactory;
import com.typesafe.config.Config;

public class Metathesaurus {

	private static final Logger LOG = LoggerFactory.getLogger(Metathesaurus.class);

	/** A graph URI to store the ontology classes */
	public static final String DEFAULT_GRAPH_URI = "http://metagraph.nicta.com/ontology/umls";

	public static final String UMLS_ENTRY_URI_BASE = "http://purl.bioontology.org/ontology/";

	public static final String UMLS_PRED_URI_BASE = "http://bioportal.bioontology.org/ontologies/umls/";

	public static final String UMLS_STY_URI_BASE = "http://bioportal.bioontology.org/ontologies/umls/sty/";

	public static final String CUI_URI_BASE = UMLS_ENTRY_URI_BASE + "umls/";

	public URI graphUri() {
		return graphUri;
	}

	public Repository getRepository() {
		return repo;
	}

	/** The maximum length of TTL string we accumulate before forcing import */
	public static final int DEFAULT_MAX_IMPORT_LENGTH = 1 << 23; // 8MiB

	private Repository repo;
	RepositoryConnection conn;
	private ValueFactory valFact;

	final URI graphUri;
	private final LocalURIs uris;
	final RepoFactory repoFactory;
	final Dataset dataset;

	private final Map<URI, SemType> semTypeCache = new HashMap<URI, SemType>();

	private boolean isVirtuoso;

	/**
	 * Construct a new queryable <code>Metathesaurus</code> instance where the
	 * Sesame repository will be created from the supplied factory, storing data
	 * in the graph <{@value #DEFAULT_GRAPH_URI}> (primarily) This constructor
	 * is useful if you want to a use non-Virtuoso backend or you don't want to
	 * use the typesafe-config library to configure your application.
	 * 
	 * If you use the recommended typesafe-config method, you can more easily
	 * use the {@link #Metathesaurus()} constructor after setting appropriate
	 * values in an <code>application.conf</code> file somewhere on the
	 * classpath.
	 * 
	 * @param repoFactory
	 *            An instance to create the repository
	 * @throws MetagraphConfigException 
	 */
	public Metathesaurus(RepoFactory repoFactory) throws MetagraphConfigException {
		this(repoFactory, DEFAULT_GRAPH_URI);
	}

	/**
	 * Construct a new queryable <code>Metathesaurus</code> instance where the
	 * Sesame repository will be created from the supplied factory. This is
	 * useful if you want to a use non-Virtuoso backend or you don't want to use
	 * the typesafe-config library to configure your application.
	 * 
	 * @param repoFactory
	 *            An instance to create the repository
	 * @param graphUri
	 *            The URI of the graph in which the entries will be stored.
	 * @param hierarchyGraphUri
	 *            The URI of the auxiliary "hierarchy" graph used to perform
	 *            faster retrieval of ancestors
	 */
	public Metathesaurus(RepoFactory repoFactory, String graphUri, String hierarchyGraphUri) {
		try {
			this.repoFactory = repoFactory;
			setUpConnection();
			this.graphUri = valFact.createURI(graphUri);
			dataset = getDataset();
			uris = new LocalURIs();
		} catch (RepositoryException e) {
			throw new RuntimeException(e);
		} catch (MetagraphConfigException e) {
			throw new RuntimeException(e);
		}

		isVirtuoso = this.repoFactory instanceof VirtuosoRepoFactory;
	}

	public Metathesaurus(Settings conf) throws MetagraphConfigException, SQLException,
			RepositoryException {
		this(conf.sesame().repoFactory(), conf.sesame().graphUri());
	}

	/**
	 * Return a new thesaurus instance based on the configuration derived from
	 * the files loaded on the classpath by the typesafe-config library
	 * 
	 * @throws MetagraphConfigException
	 * @throws SQLException
	 * @throws RepositoryException
	 */
	public Metathesaurus() throws MetagraphConfigException, SQLException, RepositoryException {
		this(new Settings());
	}

	/**
	 * Construct a new metathesaurus based on the supplied config object
	 * 
	 * @throws SQLException
	 * @throws RepositoryException
	 */
	public Metathesaurus(Config conf) throws MetagraphConfigException, SQLException,
			RepositoryException {
		this(new Settings(conf));
	}

	public Metathesaurus(RepoFactory repoFactory, String graphUri) throws MetagraphConfigException {
		this(repoFactory, graphUri, suggestHierarchyGraphUri(graphUri));
	}

	private static String suggestHierarchyGraphUri(String graphUri) throws MetagraphConfigException {
		try {
			java.net.URI uri = new java.net.URI(graphUri);
			String newHost = "hierarchy." + uri.getHost();
			java.net.URI newUri = new java.net.URI(uri.getScheme(), uri.getUserInfo(), newHost, 
					uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment());
			return newUri.toString();
		} catch (URISyntaxException e) {
			throw new MetagraphConfigException(e);
		}
	}
	
	private Dataset getDataset() {
		DatasetImpl dataset = new DatasetImpl();
		dataset.addDefaultGraph(graphUri);
		return dataset;
	}

	private void setUpConnection() throws MetagraphConfigException, RepositoryException {
		repo = repoFactory.get();
		conn = repo.getConnection();
		valFact = conn.getValueFactory();
		conn.setNamespace("umls", UMLS_PRED_URI_BASE);
		conn.setNamespace("umlssty", UMLS_STY_URI_BASE);
	}

	/** Import a file of any {@link RDFFormat} using the Sesame API */
	public void importFile(File file, RDFFormat format) throws RDFParseException,
			RepositoryException, IOException {
		LOG.info("Importing entries from {} into graph {}", file, graphUri);
		conn.add(file, "", format, graphUri);
		conn.commit();
	}

	/**
	 * Similar to {@link #importFile} except that it uses Virtuoso's native
	 * import instead of the Sesame API. This can be orders of magnitude faster.
	 * This uses an in-memory bulk import with a sensible chunk-size. Note that
	 * this is designed for TTL files produced by Metagraph, so it may not
	 * operate sensibly on others, since it makes assumptions about the file
	 * format which many valid TTL files will not conform to (in particular,
	 * statements must be separated by two newlines).
	 * 
	 * @param file
	 *            The .TTL file to import
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 */
	public void importTurtleVirtuosoNative(File file) throws SQLException, MetagraphConfigException {
		importTurtleVirtuosoNative(file, DEFAULT_MAX_IMPORT_LENGTH);
	}

	/**
	 * Similar to {@link #importFile} except that it uses Virtuoso's native
	 * import instead of the Sesame API. This can be orders of magnitude faster.
	 * 
	 * @param file
	 *            The .TTL file to import
	 * @param maxImportLength
	 *            the maximum string length to import at once;
	 *            {@value #DEFAULT_MAX_IMPORT_LENGTH} seems to work well in
	 *            practice
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 */
	public void importTurtleVirtuosoNative(File file, int maxImportLength) throws SQLException,
			MetagraphConfigException {
		// The idea here is to break the files up into chunks which the API is
		// happy with, and import them directly, instead of worrying about
		// writing to a particular location on the filesystem whcih is set
		// in AllowedDirs (configuration is annoying) or relying on Sesame
		// calls (orders of magnitude slower). A side effect is that we
		// also avoid exhausting the heap space, but in practice it seems
		// that the API is the bigger problem. It seems that sometimes
		// increasing `maxImportLength` above the default of will not make
		// the API choke but will make the import slower (Virtuoso 7.0.0/OS X)
		LOG.info("Importing entries from {} in-memory using native stored procedure calls", file);
		Scanner scan;
		try {
			scan = new Scanner(file, "UTF-8");
		} catch (FileNotFoundException e) {
			throw new MetagraphConfigException(e);
		}

		// first need to get lines starting with @prefix/@base since these are
		// needed for every chunk
		StringBuilder preambleBldr = new StringBuilder();
		String preamble = "";
		scan.useDelimiter("\\n");
		StringBuilder importable = new StringBuilder(maxImportLength);
		while (scan.hasNext()) {
			String line = scan.next().trim() + "\n";
			if (line.startsWith("#")) // ignore comments
				continue;
			if (line.isEmpty() || line.startsWith("@base ") || line.startsWith("@prefix ")) {
				preambleBldr.append(line);
				LOG.trace("Preamble line: {}", line);
			} else {
				preamble = preambleBldr.toString();
				importable.append(preamble);
				importable.append(line); // need to push the first line on as
											// well
				break;
			}
		}

		scan.useDelimiter(Pattern.compile("^$", Pattern.MULTILINE));
		while (scan.hasNext()) {
			String next = scan.next();
			if (importable.length() + next.length() >= maxImportLength) {
				LOG.debug("Importing {} characters of TTL data", importable.length());
				importStringTurtleVirtuosoDirect(importable.toString());
				LOG.trace(
						"Emptying string builder at length {} ; extra {} characters would exceed limits",
						importable.length(), next.length());
				importable = new StringBuilder(maxImportLength);
				importable.append(preamble);
			}
			if (next.length() > maxImportLength)
				throw new MetagraphConfigException(
						"Improperly formatted RDF -- terms must be separated by empty lines");
			importable.append(next);
			importable.append("\n\n");
		}
		importStringTurtleVirtuosoDirect(importable.toString());
	}

	/**
	 * Similar to {@link #importFile} except that it uses Virtuoso's native
	 * import instead of the Sesame API. This can be orders of magnitude faster,
	 * although it does require that `virtuosoImportDir` is writable and also
	 * listed under Virtuoso's `DirsAllowed` setting.
	 * {@link #importTurtleVirtuosoNative} may provide a simpler way to achieve
	 * the same result without needing to tweak Virtuoso parameters.
	 * 
	 * @param file
	 *            The .TTL file to import
	 * @param virtuosoImportDir
	 *            The directory to which the TTL File will be copied, from where
	 *            it will subsequently imported by Virtuoso
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 */
	public void importTurtleVirtuosoNativeViaDisk(File file, File virtuosoImportDir)
			throws SQLException, MetagraphConfigException {
		// prefix with a UUID in case there's another import process running
		VirtuosoRepoFactory virtRepoFactory;
		if (!isVirtuoso)
			throw new MetagraphConfigException(
					"Native imports are only supported for Virtuoso backends");
		virtRepoFactory = (VirtuosoRepoFactory) repoFactory;
		File targetFile = new File(virtuosoImportDir, UUID.randomUUID().toString() + "."
				+ file.getName());
		try {
			FileUtils.copyFile(file, targetFile);
		} catch (IOException e) {
			throw new MetagraphConfigException("Error writing to Virtuoso import directory "
					+ virtuosoImportDir);
		}
		LOG.info("Importing entries from {} on-disk using native stored procedure calls", file);
		Connection dbConn = virtRepoFactory.getJDBCConn();
		PreparedStatement call = dbConn
				.prepareCall("{call DB.DBA.TTLP_MT(file_to_string_output(?), '', ?)}");
		call.setString(1, targetFile.toString());
		call.setString(2, graphUri.stringValue());
		call.execute();
		dbConn.commit();
		targetFile.delete();
	}

	private void importStringTurtleVirtuosoDirect(String toImport) throws SQLException,
			MetagraphConfigException {
		if (!isVirtuoso)
			throw new MetagraphConfigException(
					"Native imports are only supported for Virtuoso backends");
		VirtuosoRepoFactory virtRepoFactory = (VirtuosoRepoFactory) repoFactory;
		Connection dbConn = virtRepoFactory.getJDBCConn();
		PreparedStatement call = dbConn.prepareCall("{call DB.DBA.TTLP_MT(?, '', ?)}");
		call.setString(1, toImport);
		call.setString(2, graphUri.stringValue());
		call.execute();
		dbConn.commit();
	}

	/**
	 * Imports all files from the {@code turtlePath} ending with '.ttl'. This
	 * will import via Virtuoso-native in-memory imports if the RDF repository
	 * is Virtuoso (generally the case except in testing) or by the slower
	 * Sesame imports otherwise
	 * 
	 * @param turtlePath
	 *            The path from which inputs are read.
	 * @throws IOException
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 * @throws OpenRDFException 
	 */
	public void importTurtleFromDir(File turtlePath) throws IOException, SQLException, MetagraphConfigException, OpenRDFException {
		for (File file : turtlePath.listFiles()) {
			if (!file.getName().toLowerCase().endsWith(".ttl"))
				continue;
			LOG.debug("Importing {} {} into graph {}", file, isVirtuoso ? "natively"
					: "using Sesame", graphUri);
			if (LOG.isDebugEnabled())
				LOG.debug("Repository initially has {} contexts", numContexts());
			if (isVirtuoso)
				importTurtleVirtuosoNative(file);
			else
				importFile(file, RDFFormat.TURTLE);
		}
		finishImport();
	}

	/**
	 * Imports all files from the {@code turtlePath} ending with '.ttl' using
	 * Virtuoso's native from-disk importer
	 * 
	 * @param turtlePath
	 *            The path from which inputs are read.
	 * @param virtuosoImportDir
	 *            The directory to which files will be written. Must be
	 *            configured in the {@code DirsAllowed} setting of
	 *            {@code virtuoso.ini}. If {@code null} uses a native in-memory
	 *            or Sesame import instead - like calling
	 *            {@link #importTurtleFromDir(File)}
	 * @throws IOException
	 * @throws SQLException
	 * @throws MetagraphConfigException
	 * @throws OpenRDFException 
	 */
	public void importTurtleFromDir(File turtlePath, File virtuosoImportDir)
			throws IOException, SQLException, MetagraphConfigException, OpenRDFException {
		if (virtuosoImportDir == null) {
			importTurtleFromDir(turtlePath);
		} else {
			for (File file : turtlePath.listFiles()) {
				if (!file.getName().toLowerCase().endsWith(".ttl"))
					continue;
				importTurtleVirtuosoNativeViaDisk(file, virtuosoImportDir);
			}
			finishImport();
		}
	}

	/**
	 * Note that the import has finished, enabling any necessary post-processing
	 * to happen
	 * 
	 * @throws MetagraphConfigException
	 * @throws SQLException
	 * @throws OpenRDFException 
	 */
	public void finishImport() throws SQLException, MetagraphConfigException, OpenRDFException {
		try {
			LOG.debug("Creating RDFS inferencing rules");
			try {
				setUpSubclassInferencing();
			} catch (MetagraphConfigException e) {
				LOG.info("Not explicitly creating RDFS rule set for non-virtuoso backend");
			}
			if (LOG.isDebugEnabled()) // numContexts is expensive
				LOG.debug("Repository now has {} contexts", numContexts());
		} catch (RepositoryException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void setUpSubclassInferencing() throws SQLException, MetagraphConfigException, 
			OpenRDFException {
		createRDFSRuleSet();
	}

	protected void createRDFSRuleSet() throws SQLException, MetagraphConfigException {
		VirtuosoRepoFactory virtRepoFactory;
		if (!isVirtuoso)
			throw new MetagraphConfigException(
					"Creating RDFS rule sets is only supported for Virtuoso backends right now");
		virtRepoFactory = (VirtuosoRepoFactory) repoFactory;
		Connection dbConn = virtRepoFactory.getJDBCConn();
		PreparedStatement call = dbConn.prepareCall("{call rdfs_rule_set(?, ?)}");
		call.setString(1, graphUri.stringValue());
		call.setString(2, graphUri.stringValue());
		call.execute();
		dbConn.commit();
		// dbConn.close();
	}

	public void clear() throws RepositoryException {
		LOG.debug("Clearing graph {}", graphUri);
		conn.clear(graphUri);
	}

	private URI getQualifiedUri(String prefix, String local) throws RepositoryException {
		return valFact.createURI(conn.getNamespace(prefix), local);
	}

	private TupleQuery getTupleQuery(String queryString) throws RepositoryException,
			MalformedQueryException, QueryEvaluationException {
		LOG.trace("Querying using query string: {}; ", queryString);
		TupleQuery query = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString);
		query.setDataset(dataset);
		return query;
	}

	private long numContexts() throws RepositoryException {
		return conn.size(graphUri);
	}

	/** A UMLS Semantic Type - eg T121: "Pharmacologic Substance" */
	public class SemType extends LazyGraphNode {
		private String prefLabel;
		private String nativeId;

		private SemType(URI uri) {
			super(uri);
		}

		public String prefLabel() {
			populateAllValues();
			return prefLabel;
		}

		/** Get the native ID - eg "T121" */
		public String nativeId() {
			populateAllValues();
			return nativeId;
		}

		@Override
		protected void handleLinkedPredAndObj(URI pred, String objVal) {
			if (pred.equals(uris.prefLabel))
				prefLabel = objVal;
			else if (pred.equals(uris.nativeId))
				nativeId = objVal;
		}

		@Override
		public String toString() {
			return String.format("%s: \"%s\"", nativeId(), prefLabel());
		}

	}

	/**
	 * A Metathesaurus concept, which is linked to one or more {@link #Entry}
	 * instances from the source ontologies
	 * 
	 * @author amack
	 * 
	 */
	public class Concept extends LazyGraphNode {
		private Set<URI> entryURIs = new HashSet<URI>();
		private List<Entry> entries = null;

		public Concept(URI uri) {
			super(uri);
		}

		/** Return the native ID - in this case, the CUI */
		public String nativeId() {
			if (!uri.stringValue().startsWith(CUI_URI_BASE))
				throw new RuntimeException("Entry " + uri + " must start with " + CUI_URI_BASE);
			return uri().stringValue().substring(CUI_URI_BASE.length());

		}

		@Override
		protected void handleLinkedSubjAndPred(String subjVal, URI pred) {
			if (pred.equals(uris.cui))
				entryURIs.add(valFact.createURI(subjVal));
		}

		/**
		 * Get the entries which are linked to this CUI
		 * 
		 * @return A list of {#Entry} instances
		 */
		public List<Entry> entries() {
			populateAllValues();
			if (entries != null)
				return entries;
			entries = new ArrayList<Entry>();
			for (URI entUri : entryURIs)
				entries.add(findEntry(entUri));
			return entries;
		}

		public String toString() {
			return nativeId();
		}

	}

	/**
	 * An entry in one of the source ontologies of Metathesaurus
	 * 
	 * @author amack
	 * 
	 */
	public class Entry extends LazyGraphNode {
		private List<URI> cuiUris = new ArrayList<URI>();
		private List<String> altLabels = new ArrayList<String>();

		private String prefLabel = null;
		private final String nativeId;
		private final String sourceKey;
		private List<URI> semTypeUris = new ArrayList<URI>();
		private List<SemType> semTypes = null;

		private MultiMap<String, Entry> sourceSpecificRels = new MultiHashMap<String, Entry>();
		
		/** Keep track of the populated fields when in partial eager mode */
		private Set<EntryAuxField> populatedFields = new HashSet<EntryAuxField>();
		
		private Entry(URI uri) {
			super(uri);
			String[] comps = prefixedNativeId().split("/");
			if (comps.length != 2)
				throw new InvalidURIException("URI Suffix " + prefixedNativeId() + " is invalid");
			sourceKey = comps[0];
			nativeId = comps[1];
		}
		
		/** Populates the entry eagerly but, if <code>eagerFields != null</code>,
		 * notes that the supplied predicate pairs <code>inbound</code> and <code>outbound</code>
		 * only supply values for the given fields.
		 * 
		 * @param uri 
		 * @param inbound
		 * @param outbound
		 * @param eagerFields
		 */
		private Entry(URI uri, Collection<SubjectPredPair> inbound,
				Collection<PredObjectPair> outbound, 
				Collection<EntryAuxField> eagerFields) {
			this(uri);
			populateFromMaps(inbound, outbound);
			if (eagerFields != null) {
				setFullyPopulated(false);
				populatedFields.addAll(eagerFields);
			}
		}
		
		private boolean fieldIsPopulated(EntryAuxField field) {
			return isFullyPopulated() || populatedFields.contains(field);
		}

		/**
		 * Return the URIs of the CUIs associated with the entry. Mostly useful
		 * if you want to manually query the graph of the CUI.
		 * 
		 * @return A list of CUI node URIs
		 */
		public List<URI> cuiUris() {
			if (!fieldIsPopulated(EntryAuxField.CUI))
				populateAllValues();
			return cuiUris;
		}

		public String getSourceKey() {
			return sourceKey;
		}

		/**
		 * Return the raw CUIs (Metathesaurus identifiers) of the entry
		 * 
		 * @return A list of CUI strings
		 */
		public List<String> cuis() {
			List<String> cuis = new ArrayList<String>();
			for (URI cu : cuiUris())
				cuis.add(cu.getLocalName());
			return cuis;
		}

		/**
		 * Return any alternate labels listed for the entry
		 * 
		 * @return A list of alternate labels (synonyms)
		 */
		public List<String> altLabels() {
			if (!fieldIsPopulated(EntryAuxField.ALT_LABEL))
				populateAllValues();
			return altLabels;
		}

		/**
		 * Return the preferred label of the entry.
		 * 
		 * @return The preferred canonical label of the entry
		 */
		public String prefLabel() {
			if (!fieldIsPopulated(EntryAuxField.PREF_LABEL))
				populateAllValues();
			return prefLabel;
		}

		/**
		 * Return the semantic types of this entry
		 * 
		 * @return A list of semantic type objects
		 */
		public List<SemType> semTypes() {
			if (!fieldIsPopulated(EntryAuxField.SEM_TYPE))
				populateAllValues();
			if (semTypes != null)
				return semTypes;
			semTypes = new ArrayList<SemType>();
			for (URI semTypeUri : semTypeUris)
				semTypes.add(findSemType(semTypeUri));
			return semTypes;
		}

		/**
		 * Return the semantic types IDs of this entry
		 * 
		 * @return A list of semantic type IDs - eg T121
		 */
		public List<String> semTypeIds() {
			if (!fieldIsPopulated(EntryAuxField.SEM_TYPE))
				populateAllValues();
			List<String> stIds = new ArrayList<String>();
			for (URI semTypeUri : semTypeUris)
				stIds.add(semTypeUri.getLocalName());
			return stIds;
		}

		/**
		 * Return a mapping from relation names to entries which are related to
		 * this one in a manner which is unique to the source (e.g.
		 * MSH/mapped_to, SNOMEDCT/may_be_a)
		 * 
		 * @return
		 */
		public MultiMap<String, Entry> sourceSpecificRels() {
			populateAllValues();
			return sourceSpecificRels;
		}

		private List<Entry> entriesFromQuery(TupleQuery query, String entryUriBinding)
				throws QueryEvaluationException {
			return entriesFromQuery(query, entryUriBinding, false);
		}

		private List<Entry> entriesFromQuery(TupleQuery query, String entryUriBinding,
				boolean excludeSelf) throws QueryEvaluationException {
			List<URI> entryUris = entryURIsFromQuery(query, entryUriBinding, excludeSelf);
			List<Entry> entries = new ArrayList<Entry>();
			for (URI uri : entryUris)
				entries.add(new Entry(uri));
			return entries;
		}
		
		private List<Entry> entriesFromQueryEager(TupleQuery query, String entryUriBinding,
				boolean excludeSelf, Collection<EntryAuxField> eagerFields) throws QueryEvaluationException, RepositoryException,
				MalformedQueryException {
			List<URI> entryUris = entryURIsFromQuery(query, entryUriBinding, excludeSelf);
			if (entryUris.size() == 0)
				return new ArrayList<Entry>(0); // saves time but more importantly stops syntax error
			String urisForQuery = sparqlUriList(entryUris);
			String genFilter = String.format("?ent IN ( %s )", urisForQuery);
			if (eagerFields != null) {
				List<URI> predUris = new ArrayList<URI>();
				for (EntryAuxField f : eagerFields)
					predUris.add(auxFieldToUri(f));
				String predUrisForQuery = sparqlUriList(predUris);
				genFilter += String.format(" && ?p IN ( %s )", predUrisForQuery);
			}
			
			String outbQS = String.format("SELECT * WHERE { { ?ent ?p ?o } FILTER ( %s ) }", 
					genFilter);
			TupleQuery outbQuery = getTupleQuery(outbQS);
			MultiMap<URI, PredObjectPair> urisToObjects = new MultiHashMap<URI, PredObjectPair>();
			TupleQueryResult outbResult = outbQuery.evaluate();
			while (outbResult.hasNext()) {
				BindingSet bs = outbResult.next();
				URI pred = valFact.createURI(bs.getBinding("p").getValue().stringValue());
				String objVal = bs.getBinding("o").getValue().stringValue();
				URI entUri = valFact.createURI(bs.getBinding("ent").getValue().stringValue());
				urisToObjects.put(entUri, new PredObjectPair(pred, objVal));
			}
			String inbQS = String.format("SELECT * WHERE { { ?s ?p ?ent }  FILTER ( %s ) }",
					genFilter);
			TupleQuery inbQuery = getTupleQuery(inbQS);
			MultiMap<URI, SubjectPredPair> urisToSubjects = new MultiHashMap<URI, SubjectPredPair>();
			TupleQueryResult inbResult = inbQuery.evaluate();
			while (inbResult.hasNext()) {
				BindingSet bs = inbResult.next();
				String subjVal = bs.getBinding("s").getValue().stringValue();
				URI pred = valFact.createURI(bs.getBinding("p").getValue().stringValue());
				URI entUri = valFact.createURI(bs.getBinding("ent").getValue().stringValue());
				urisToSubjects.put(entUri, new SubjectPredPair(subjVal, pred));
			}

			List<Entry> entries = new ArrayList<Entry>();
			for (URI entUri : entryUris) {
				Collection<SubjectPredPair> inbound = urisToSubjects.get(entUri);
				Collection<PredObjectPair> outbound = urisToObjects.get(entUri);
				if (inbound == null)
					inbound = new ArrayList<SubjectPredPair>(0);
				if (outbound == null)
					outbound = new ArrayList<PredObjectPair>(0);
				entries.add(new Entry(entUri, inbound, outbound, eagerFields));
			}
			return entries;
		}

		private List<URI> entryURIsFromQuery(TupleQuery query, String entryUriBinding,
				boolean excludeSelf) throws QueryEvaluationException {
			List<URI> entryUris = new ArrayList<URI>();
			TupleQueryResult result = query.evaluate();
			while (result.hasNext()) {
				BindingSet bs = result.next();
				String ent = bs.getBinding(entryUriBinding).getValue().stringValue();
				if (excludeSelf && ent.equals(uri.stringValue()))
					continue;
				entryUris.add(valFact.createURI(ent));
			}
			result.close();
			return entryUris;
		}

		/**
		 * Return parents (immediate ancestors) from the ontology. There may be
		 * multiple inheritance, so multiple entries can be returned even though
		 * we only look one level up in the hierarchy
		 */
		public List<Entry> parents() {
			return parents(false);
		}

		/**
		 * Return parents (immediate ancestors) from the ontology. There may be
		 * multiple inheritance, so multiple entries can be returned even though
		 * we only look one level up in the hierarchy
		 * 
		 * @param eager
		 *            if {@code true}, eagerly populate attributes of the
		 *            fetched parents. This can save time since it requires
		 *            fewer queries.
		 */
		public List<Entry> parents(boolean eager) {
			return ancestors(eager, true);
		}

		/**
		 * Get all direct and indirect ancestors - that is, any entry, from any
		 * loaded source ontology, which this Entry is a subclass of
		 */
		public List<Entry> ancestors() {
			return ancestors(false);
		}

		/**
		 * Get all direct and indirect ancestors - that is, any entry, from any
		 * loaded source ontology, which this Entry is a subclass of
		 * 
		 * @param eager
		 *            if {@code true}, eagerly fetch populate attributes of the
		 *            fetched ancestors. This can save time since it requires
		 *            fewer queries.
		 */
		public List<Entry> ancestors(boolean eager) {
			return ancestors(eager, false);
		}

		/**
		 * Return direct and indirect ancestors from the ontology. This method
		 * allows specifying a set of "eager fields" which are the only ones we are interested
		 * in, and which will be fetched straight away. (Other fields will be loaded
		 * lazily if the fields are requested). This can save a lot of time over either 
		 * lazy loading or fully eager loading since it minimised the number of DB calls
		 * and the amount of data pulled from the DB
		 * 
		 * @param eagerFields These and only these fields will be fetched eagerly.
		 */
		public List<Entry> ancestors(Collection<EntryAuxField> eagerFields) {
			return ancestors(eagerFields, false);
		}

		
		/**
		 * Get all direct and (if <code>directOnly == false</code>) indirect
		 * ancestors - that is, any entry, from any loaded source ontology,
		 * which this Entry is a subclass of
		 */
		public List<Entry> ancestors(boolean eager, boolean directOnly) {
			return ancestors(eager, directOnly, null);
		}

		/**
		 * Get all direct and (if <code>directOnly == false</code>) indirect
		 * ancestors. This method allows specifying a set of "eager fields" 
		 * which are the only ones we are interested
		 * in, and which will be fetched straight away (Other fields will be loaded
		 * lazily if the fields are requested). This can save a lot of time over either 
		 * lazy loading or fully eager loading since it minimised the number of DB calls
		 * and the amount of data pulled from the DB
		 * 
		 * @param eagerFields These and only these fields will be fetched eagerly.
		 * @param directOnly Whether to return only direct parents or all ancestors.
		 */
		public List<Entry> ancestors(Collection<EntryAuxField> eagerFields, boolean directOnly) {
			return ancestors(true, directOnly, eagerFields);
		}

		
		/**
		 * Get all direct and (if <code>directOnly == false</code>) indirect
		 * ancestors - that is, any entry, from any loaded source ontology,
		 * which this Entry is a subclass of
		 * 
		 * @param eager
		 *            if {@code true}, eagerly populate attributes of the
		 *            fetched ancestors. This can save time since it requires
		 *            fewer queries.
		 * @param directOnly Whether to return only direct children or all descendants.
		 * @param eagerFields If <code>eager == true</code>, these and only these fields will be fetched eagerly.
		 */
		private List<Entry> ancestors(boolean eager, boolean directOnly, Collection<EntryAuxField> eagerFields) {
			try {
				String queryString = "SELECT DISTINCT ?ancestor " +
						" WHERE { " + getAncestorWhereClause(directOnly) + " }";
				LOG.trace("Ancestor query string: {}", queryString);
				TupleQuery query = getTupleQuery(queryString);
				query.setBinding("this", uri);
				if (eager)
					return entriesFromQueryEager(query, "ancestor", false, eagerFields);
				else
					return entriesFromQuery(query, "ancestor");
			} catch (OpenRDFException e) {
				throw new RuntimeException(e);
			}
		}
		
		private String getAncestorWhereClause(boolean directOnly) {
			// virtuoso <= 6.1.5 can't handle sparql property paths
			// (http://www.w3.org/TR/sparql11-property-paths/), getting a syntax error
			// The '+' in rdfs:subClassOf+ is an instance of this,
			// but since it's not supported in common Virtuoso versions 
			// (6.1.6+ and 7.0+ are fine though)
			// we have to use this worse syntax
			String suff = isVirtuoso && !directOnly ? 
				"OPTION(transitive, t_distinct, t_no_cycles)" : "";
			String pred = isVirtuoso || directOnly ? "rdfs:subClassOf" : "rdfs:subClassOf+";
			return "?this " + pred + " ?ancestor " + suff;
		}

		/**
		 * Return children (immediate descendants) from the ontology.
		 */
		public List<Entry> children() {
			return children(false);
		}

		/**
		 * Return children (immediate descendants) from the ontology.
		 * 
		 * @param eager
		 *            if {@code true}, eagerly populate attributes of the
		 *            fetched ancestors. This can save time since it requires
		 *            fewer queries.
		 */
		public List<Entry> children(boolean eager) {
			return descendants(eager, true);
		}

		/**
		 * Return children (immediate descendants) from the ontology.
		 * 
		 * @param eager
		 *            if {@code true}, eagerly populate attributes of the
		 *            fetched ancestors. This can save time since it requires
		 *            fewer queries.
		 * @param directOnly Whether to return only direct children or all descendants.
		 */
		public List<Entry> descendants(boolean eager, boolean directOnly) {
			return descendants(eager, directOnly, null);
		}

		/**
		 * Get all direct and (if <code>directOnly == false</code>) indirect
		 * ancestors. This method
		 * allows specifying a set of "eager fields" which are the only ones we are interested
		 * in, and which will be fetched straight away. (Other fields will be loaded
		 * lazily if the fields are requested). This can save a lot of time over either 
		 * lazy loading or fully eager loading since it minimised the number of DB calls
		 * and the amount of data pulled from the DB
		 * 
		 * @param eagerFields These and only these fields will be fetched eagerly.
		 * @param directOnly Whether to return only direct children or all descendants.
		 */
		public List<Entry> descendants(Collection<EntryAuxField> eagerFields, boolean directOnly) {
			return descendants(true, directOnly, eagerFields);
		}

		
		/**
		 * Return direct and indirect descendants from the ontology. This method
		 * allows specifying a set of "eager fields" which are the only ones we are interested
		 * in, and which will be fetched straight away. (Other fields will be loaded
		 * lazily if the fields are requested). This can save a lot of time over either 
		 * lazy loading or fully eager loading since it minimised the number of DB calls
		 * and the amount of data pulled from the DB
		 * 
		 * @param eagerFields These and only these fields will be fetched eagerly.
		 * @param directOnly Whether to return only direct children or all descendants.
		 */
		public List<Entry> descendants(Collection<EntryAuxField> eagerFields) {
			return descendants(eagerFields, false);
		}

		/**
		 * Get all direct and indirect descendants - that is, any entry, from
		 * any loaded source ontology, which is a subclass of this Entry
		 */
		public List<Entry> descendants() {
			return descendants(false);
		}

		/**
		 * Get all direct and indirect descendants - that is, any entry, from
		 * any loaded source ontology, which is a subclass of this Entry
		 */
		public List<Entry> descendants(boolean eager) {
			return descendants(eager, false);
		}

		/**
		 * Get all direct and (if <code>directOnly == false</code>) indirect
		 * descendants - that is, any entry, from any loaded source ontology,
		 * which is a subclass of this Entry. 
		 * @param eager
		 *            if {@code true}, eagerly populate attributes of the
		 *            fetched ancestors. This can save time since it requires
		 *            fewer queries.
		 * @param directOnly Whether to return only direct children or all descendants.
		 * @param eagerFields If <code>eager == true</code>, these and only these fields will be fetched eagerly.
		 */
		private List<Entry> descendants(boolean eager, boolean directOnly, Collection<EntryAuxField> eagerFields) {
			try {
				String queryString = "SELECT DISTINCT ?descendant " + 
						" WHERE { " + getDescendantWhereClause(directOnly) + " }";
				LOG.trace("Descendant query string: {}", queryString);
				TupleQuery query = getTupleQuery(queryString);
				query.setBinding("this", uri);
				if (eager)
					return entriesFromQueryEager(query, "descendant", false, eagerFields);
				else
					return entriesFromQuery(query, "descendant");
			} catch (OpenRDFException e) {
				throw new RuntimeException(e);
			}
		}
		
		private String getDescendantWhereClause(boolean directOnly) {
			String suff = isVirtuoso && !directOnly ? 
				"OPTION(transitive, t_distinct, t_no_cycles)" : "";
			String pred = isVirtuoso || directOnly ? "rdfs:subClassOf" : "rdfs:subClassOf+";
			return "?descendant " + pred + " ?this " + suff;
		}


		/**
		 * Get a unique ID (derived from the URI) which is guaranteed to
		 * identify the Metamap entry from other entries; takes a form such as
		 * 'ICD10/S66.8'
		 * 
		 * @return a unique ID
		 */
		public String prefixedNativeId() {
			if (!uri().stringValue().startsWith(UMLS_ENTRY_URI_BASE))
				throw new InvalidURIException("Entry " + uri() + " must start with "
						+ UMLS_ENTRY_URI_BASE);
			return uri().stringValue().substring(UMLS_ENTRY_URI_BASE.length());
		}

		/**
		 * Return the native ID of the entity in the source ontology -- eg
		 * "S66.8", "D04357"
		 */
		public String bareNativeId() {
			populateAllValues();
			return nativeId;
		}

		public String toString() {
			String label = isFullyPopulated() ? prefLabel() : "???";
			return String.format("%s: \"%s\"", prefixedNativeId(), label);
		}

		/** Like toString(), but forces values to be populated first */
		public String toPopulatedString() {
			populateAllValues();
			return toString();
		}

		@Override
		protected void handleLinkedPredAndObj(URI pred, String objVal) {
			if (pred.equals(uris.cui) && objVal.startsWith(CUI_URI_BASE))
				cuiUris.add(valFact.createURI(objVal));
			else if (pred.equals(uris.prefLabel))
				prefLabel = objVal;
			else if (pred.equals(uris.altLabel))
				altLabels.add(objVal);
			else if (pred.equals(uris.semType))
				semTypeUris.add(valFact.createURI(objVal));
			else if (pred.getNamespace().equals(uri.getNamespace()))
				sourceSpecificRels.put(pred.getLocalName(), new Entry(valFact.createURI(objVal)));
		}
		
		private URI auxFieldToUri(EntryAuxField af) {
			switch (af) {
			case CUI:
				return uris.cui;
			case PREF_LABEL:
				return uris.prefLabel;
			case ALT_LABEL:
				return uris.altLabel;
			case SEM_TYPE:
				return uris.semType;
			default:
				return null;
			}
		}
		
	}

	/** An enum which clients can use to specify which 
	 * values to retrieve when fetching ancestors or 
	 * descendants. 
	 * @author amack
	 *
	 */
	public enum EntryAuxField {
		CUI,
		PREF_LABEL,
		ALT_LABEL,
		SEM_TYPE
	}

	
	private class LocalURIs {
		private final URI cui;
		private final URI prefLabel;
		private final URI altLabel;
		private final URI nativeId;
		private final URI semType;

		private LocalURIs() throws RepositoryException {
			cui = getQualifiedUri("skos", "closeMatch");
			prefLabel = getQualifiedUri("skos", "prefLabel");
			altLabel = getQualifiedUri("skos", "altLabel");
			nativeId = getQualifiedUri("skos", "notation");
			semType = getQualifiedUri("umls", "hasSTY");
		}
	}

	abstract class LazyGraphNode {
		protected final URI uri;
		private boolean isFullyPopulated = false;

		public LazyGraphNode(URI uri) {
			this.uri = uri;
		}

		/** The canonical URI of the graph node */
		public URI uri() {
			return uri;
		}

		protected boolean isFullyPopulated() {
			return isFullyPopulated;
		}
		
		protected void setFullyPopulated(boolean value) {
			isFullyPopulated = value;
		}

		protected void populateAllValues() {
			if (isFullyPopulated)
				return;
			try {
				populateFromOutbound();
				populateFromInbound();
				isFullyPopulated = true;
			} catch (OpenRDFException e) {
				throw new RuntimeException(e);
			}
		}

		protected void populateFromMaps(Collection<SubjectPredPair> inbound,
				Collection<PredObjectPair> outbound) {
			for (SubjectPredPair subjPred : inbound)
				handleLinkedSubjAndPred(subjPred.subject, subjPred.predicate);
			for (PredObjectPair predObj : outbound)
				handleLinkedPredAndObj(predObj.predicate, predObj.object);
			isFullyPopulated = true;
		}

		private void populateFromInbound() throws OpenRDFException {
			TupleQueryResult result = getRawInboundPredicates();
			while (result.hasNext()) {
				BindingSet bs = result.next();
				URI pred = valFact.createURI(bs.getBinding("p").getValue().stringValue());
				String subjVal = bs.getBinding("s").getValue().stringValue();
				handleLinkedSubjAndPred(subjVal, pred);
			}
		}

		private void populateFromOutbound() throws OpenRDFException {
			TupleQueryResult result = getRawOutboundPredicates();
			while (result.hasNext()) {
				BindingSet bs = result.next();
				URI pred = valFact.createURI(bs.getBinding("p").getValue().stringValue());
				String objVal = bs.getBinding("o").getValue().stringValue();
				handleLinkedPredAndObj(pred, objVal);
			}
		}

		protected void handleLinkedPredAndObj(URI pred, String objVal) {
		}

		protected void handleLinkedSubjAndPred(String subjVal, URI pred) {
		}
		
		// START HERE  - try and figure out how we can selectively mark
		// fields as populated

		/**
		 * Low-level access to all outbound links from this URI in the Sesame
		 * graph
		 * 
		 * @throws OpenRDFException
		 */
		public TupleQueryResult getRawOutboundPredicates() throws OpenRDFException {
			TupleQuery query = getTupleQuery("SELECT * WHERE { ?this ?p ?o }");
			query.setBinding("this", uri);
			return query.evaluate();
		}

		/**
		 * Low-level access to all inbound links from this URI in the Sesame
		 * graph
		 * 
		 * @throws OpenRDFException
		 */
		public TupleQueryResult getRawInboundPredicates() throws OpenRDFException {
			TupleQuery query = getTupleQuery("SELECT * WHERE { ?s ?p ?this }");
			query.setBinding("this", uri);
			return query.evaluate();
		}

		@Override
		public boolean equals(Object other) {
			try {
				LazyGraphNode otherGN = (LazyGraphNode) other;
				return otherGN.uri.equals(uri);
			} catch (ClassCastException e) {
				return false;
			}
		}

	}

	/** Find the semantic type with the given URI */
	public SemType findSemType(URI semTypeUri) {
		SemType existing = semTypeCache.get(semTypeUri);
		if (existing != null)
			return existing;
		checkIsClass(semTypeUri);
		SemType newInst = new SemType(semTypeUri);
		semTypeCache.put(semTypeUri, newInst);
		return newInst;
	}

	/** Find the semantic type object by a semantic type ID such as "T121" */
	public SemType findSemTypeById(String semTypeId) {
		return findSemType(uriForSemTypeId(semTypeId));
	}

	private void checkIsClass(URI uri) {
		try {
			TupleQuery query = getTupleQuery("SELECT * WHERE { ?s a owl:Class }");
			query.setBinding("s", uri);
			TupleQueryResult result = query.evaluate();
			if (!result.hasNext())
				throw new NotFoundException("Could not locate URI <" + uri + "> in <" + graphUri
						+ ">");
		} catch (OpenRDFException e) {
			throw new RuntimeException(e);
		}
	}

	private void checkIsTarget(URI uri) {
		try {
			TupleQuery query = getTupleQuery("SELECT ?s WHERE { ?s ?p ?o } LIMIT 1");
			query.setBinding("o", uri);
			TupleQueryResult result = query.evaluate();
			if (!result.hasNext())
				throw new NotFoundException("Could not locate URI <" + uri + "> in <" + graphUri
						+ ">");
		} catch (OpenRDFException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Find an entry by the key of the source ontology and its native identifier
	 * 
	 * @param sourceKey
	 *            The identifier of the source ontology - eg "MSH", "ICD10",
	 *            "SNOMEDCT"
	 * @param nativeId
	 *            The native ID in the source ontology - eg "C057026", "S66.8",
	 *            "251415007"
	 * @return An entry with the specified ID from the specified ontology
	 */
	public Entry findEntryByNativeId(String sourceKey, String nativeId) {
		return findEntry(uriForEntry(sourceKey, nativeId));
	}

	/**
	 * Find an entry by the "prefixed ID" - the key of the source ontology and
	 * its native identifier separated by '/'
	 * 
	 * @param prefixedId
	 *            The prefixed native ID (SOURCE/ID) - eg "MSH/C057026",
	 *            "ICD10/S66.8", "SNOMEDCT/251415007"
	 * @return An entry with the specified ID from the specified ontology
	 */
	public Entry findEntryByPrefixedId(String prefixedId) {
		return findEntry(uriForEntryPrefixedId(prefixedId));
	}

	/** Find an entry by its URI */
	public Entry findEntry(URI entUri) {
		checkIsClass(entUri);
		return new Entry(entUri);
	}

	/**
	 * Find a UMLS concept by its URI
	 * 
	 * @param concUri
	 *            The canonical URI of the concept
	 */
	public Concept findConcept(URI concUri) {
		checkIsTarget(concUri);
		return new Concept(concUri);
	}

	/** Find a UMLS concept by its CUI - eg "C0429254" */
	public Concept findConceptByCui(String cui) {
		return findConcept(uriForCui(cui));
	}

	/**
	 * Retrieve all entries from the currently loaded data which align with the
	 * provided CUI; equivalent to {@link #findConceptByCui(cui)}.entries()
	 * 
	 * @param cui
	 *            The CUI of the concept
	 * @return a list of relevant methesaurus {#Entry}s
	 */
	public List<Entry> findEntriesByCui(String cui) {
		List<Entry> ents = new ArrayList<Entry>();
		try {
			URI cuiUri = uriForCui(cui);
			TupleQuery query = getTupleQuery("SELECT ?s WHERE { ?s skos:closeMatch ?cuiUri }");
			query.setBinding("cuiUri", cuiUri);
			TupleQueryResult result = query.evaluate();
			while (result.hasNext()) {
				BindingSet bs = result.next();
				Value subj = bs.getBinding("s").getValue();
				ents.add(new Entry(valFact.createURI(subj.stringValue())));
			}
			result.close();
		} catch (OpenRDFException e) {
			throw new RuntimeException(e);
		}
		return ents;
	}

	/** Return a URI for the CUI - useful for direct graph querying */
	public URI uriForCui(String cui) {
		return valFact.createURI(CUI_URI_BASE, cui);
	}

	/**
	 * Return a URI for the semantic type ID (eg "T121") - useful for direct
	 * graph querying
	 */
	public URI uriForSemTypeId(String semTypeId) {
		try {
			return getQualifiedUri("umlssty", semTypeId);
		} catch (RepositoryException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Return a URI for the prefixed entry ID (eg "ICD10/S66.8") - useful for
	 * direct graph querying
	 */
	public URI uriForEntryPrefixedId(String prefixedId) {
		return valFact.createURI(UMLS_ENTRY_URI_BASE, prefixedId);
	}
	
	private static String sparqlUriList(Collection<URI> uris) {
		return Util.join(Util.addAffixes(uris, "<", ">"), ", ");
	}

	/**
	 * Return a URI for the (source ontology, native ID) pair - useful for
	 * direct graph querying
	 */
	public URI uriForEntry(String sourceKey, String nativeId) {
		return uriForEntryPrefixedId(String.format("%s/%s", sourceKey, nativeId));
	}

	private class PredObjectPair {
		private final URI predicate;
		private final String object;

		private PredObjectPair(URI predicate, String object) {
			this.predicate = predicate;
			this.object = object;
		}
	}

	private class SubjectPredPair {
		private final String subject;
		private final URI predicate;

		private SubjectPredPair(String subject, URI predicate) {
			this.subject = subject;
			this.predicate = predicate;
		}
	}
}
