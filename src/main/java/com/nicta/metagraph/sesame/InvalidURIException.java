package com.nicta.metagraph.sesame;

public class InvalidURIException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidURIException(String msg) {
		super(msg);
	}

	public InvalidURIException(Throwable e) {
		super(e);
	}

}
