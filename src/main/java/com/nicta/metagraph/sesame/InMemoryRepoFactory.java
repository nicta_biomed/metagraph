package com.nicta.metagraph.sesame;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.openrdf.sail.memory.MemoryStore;

import com.nicta.metagraph.MetagraphConfigException;

/** A repository factory which produces a singleton in-memory instance per VM.
 * This class is mostly used for testing, although you may be able to use it to store small ontologies
 * @author amack
 *
 */
public class InMemoryRepoFactory implements RepoFactory {
	static Repository repo = null;

	public InMemoryRepoFactory() throws RepositoryException {
		if (repo == null) {
			repo = new SailRepository(new ForwardChainingRDFSInferencer(new MemoryStore()));
			repo.initialize();
		}
		RepositoryConnection conn = repo.getConnection();
		// add in the NSs which Virtuoso connections already have
		conn.setNamespace("skos", "http://www.w3.org/2004/02/skos/core#");
		conn.setNamespace("owl", "http://www.w3.org/2002/07/owl##");
		conn.setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		conn.setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#");
	}

	@Override
	public Repository get() throws MetagraphConfigException {
		return repo;
	}

}
