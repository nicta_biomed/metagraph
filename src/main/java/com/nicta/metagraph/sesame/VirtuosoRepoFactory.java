package com.nicta.metagraph.sesame;

import java.sql.Connection;
import java.sql.SQLException;

import org.openrdf.repository.Repository;

import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.sesame2.driver.VirtuosoRepository;

import com.nicta.metagraph.MetagraphConfigException;

public class VirtuosoRepoFactory implements RepoFactory {
	public final String serverName;
	public final int port;
	public final String username;
	public final String password;
	private Connection jdbcConn;
	private Repository repo;
	
	private static final String ENCODING = "UTF-8";

	public VirtuosoRepoFactory(String serverName, int port, String username, String password)
			throws SQLException, MetagraphConfigException {
		this.serverName = serverName;
		this.port = port;
		this.username = username;
		this.password = password;
		jdbcConn = newJDBCConn();
		repo = getNew();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nicta.metagraph.sesame.RepoFactory#getJDBCConnString()
	 */
	public String getJDBCConnString() {
		return String.format("jdbc:virtuoso://%s:%d/CHARSET=%s", serverName, port, ENCODING);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nicta.metagraph.sesame.RepoFactory#getJDBCConn()
	 */
	private Connection newJDBCConn() throws SQLException, MetagraphConfigException {
		VirtuosoDataSource vds = new VirtuosoDataSource();
		vds.setServerName(serverName);
		vds.setPortNumber(port);
		vds.setUser(username);
		vds.setPassword(password);
		vds.setCharset(ENCODING);
		return vds.getConnection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nicta.metagraph.sesame.RepoFactory#getJDBCConn()
	 */
	public Connection getJDBCConn() throws SQLException, MetagraphConfigException {
		return jdbcConn;
	}

	private Repository getNew() throws MetagraphConfigException {
		// lazy add parameter attempting to prevent error
		// "SR491: too many open statements" when importing prototypes
		return new VirtuosoRepository(getJDBCConnString(), username, password, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.nicta.metagraph.sesame.RepoFactory#get()
	 */
	@Override
	public Repository get() {
		return repo;
	}
}
