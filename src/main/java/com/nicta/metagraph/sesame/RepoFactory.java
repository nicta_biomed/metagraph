package com.nicta.metagraph.sesame;

import org.openrdf.repository.Repository;

import com.nicta.metagraph.MetagraphConfigException;

public interface RepoFactory {

	/** Return a repository instance for storing RDF triples.
	 * 
	 * This should talk underlyingly to the same repository if repeated calls are made 
	 * (although need not be the same instance) */
	public abstract Repository get() throws MetagraphConfigException;

}