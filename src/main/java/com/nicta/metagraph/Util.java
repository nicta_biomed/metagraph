package com.nicta.metagraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Util {
	public static Collection<String> addAffixes(Collection<?> s, String prefix, String suffix) {
		List<String> wrapped = new ArrayList<String>(s.size());
		for (Object elem : s) 
			wrapped.add(prefix + elem + suffix);
		return wrapped;
	}
	
	public static String join(Iterable<?> s, String delimiter) {
		StringBuilder builder = new StringBuilder();
		Iterator<?> iter = s.iterator();
		while (iter.hasNext()) {
			builder.append(iter.next());
			if (!iter.hasNext()) 
				break;
			builder.append(delimiter);
		}
		return builder.toString();
	}
}
