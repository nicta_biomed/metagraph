package com.nicta.metagraph;

public class MetagraphConfigException extends Exception {

	public MetagraphConfigException(String msg) {
		super(msg);
	}
	
	public MetagraphConfigException(Throwable e) {
		super(e);
	}

}
