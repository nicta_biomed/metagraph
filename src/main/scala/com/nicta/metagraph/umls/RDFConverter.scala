package com.nicta.metagraph.umls

/**
 * **
 * The code in this file is adapted from NCBOs umls2rdf tool
 * (https://github.com/ncbo/umls2rdf/)
 *
 * The code was ported to Scala and heavily customised by Andrew MacKinlay of National ICT Australia
 *
 * The following licence applies to code in this file:
 *
 * Copyright 2013, National ICT Australia
 *
 * Copyright 2005???2011, The Board of Trustees of Leland Stanford Junior University. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY <COPYRIGHT HOLDER> ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of The Board of Trustees of Leland Stanford Junior University.
 *
 *
 */

import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.io.BufferedWriter
import java.io.FileWriter
import java.lang.{ Iterable => JIterable }
import java.net.URLEncoder
import java.io.FileNotFoundException
import java.io.File
import collection.mutable
import mutable.{ Map => MMap, Set => MSet, ArrayBuffer, StringBuilder }
import io.Source
import org.slf4j.LoggerFactory
import com.nicta.metagraph.{ MissingUMLSDataException, MetagraphConfigException }
import resource.managed
import java.util.zip.GZIPInputStream
import java.io.FileInputStream
import org.apache.commons.io.FileUtils
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.forkjoin.ForkJoinPool

object RDFConverter {
  import Metathesaurus.{ UMLS_ENTRY_URI_BASE, UMLS_STY_URI_BASE, UMLS_PRED_URI_BASE }

  private val LOG = LoggerFactory.getLogger(getClass())

  private val ENC = "utf-8"

  private val PREFIXES = """
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix owl:  <http://www.w3.org/2002/07/owl#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix umls: <%1$s> .
@prefix umlssty: <%2$s> .
@base <%3$s> .


""".format(UMLS_PRED_URI_BASE, UMLS_STY_URI_BASE, UMLS_ENTRY_URI_BASE)

  private val ONTOLOGY_HEADER = """
<%s>
    a owl:Ontology ;
    rdfs:comment "%s" ;
    rdfs:label "%s" ;
    owl:imports <http://www.w3.org/2004/02/skos/core> ;
    owl:versionInfo "%s" .

"""

  def getUmlsUrl(code: String): String = code

  def escape(string: String): String = {
    string.replace("""\""", """\\""").replace("\"", "\\\"")
  }

  def getUrlTerm(ns: String, code: String): String = {
    val sep = if (Set('/', ':') contains ns(ns.size - 1)) "" else '/'
    "<%s%s%s>".format(ns, sep, URLEncoder.encode(code, ENC))
  }

  // http://www.nlm.nih.gov/research/umls/sourcereleasedocs/current/SNOMEDCT/relationships.html

  def getRRFSource(dataRoot: File, tableName: String) = {
    val stem = "%s.RRF".format(tableName)
    val rrfPathBase = new File(dataRoot, stem);
    val rrfPathGz = new File(dataRoot, stem + ".gz");
    if (rrfPathBase.exists()) {
      Source.fromFile(rrfPathBase, "UTF-8")
    } else if (rrfPathGz.exists()) {
      val gzi = new GZIPInputStream(new FileInputStream(rrfPathGz))
      Source.fromInputStream(gzi, "UTF-8")
    } else { 
      throw new FileNotFoundException(s"File $rrfPathBase (or variant $rrfPathGz) not found");
    }
  }

  class UmlsFieldIndices(dataRoot: File) {
    import UmlsFieldIndices._

    val tableNamesToFieldLists: Map[String, Seq[String]] = {
      val src = getRRFSource(dataRoot, "MRFILES")
      val lines = src.getLines
      val entries = lines map { line =>
        val comps = line.split('|')
        val tableName = comps(0).split('.')(0)
        val fields = comps(2).split(',').toSeq
        (tableName, fields)
      }
      val namesToFields = entries.toMap
      src.close
      namesToFields
    }

    abstract class UmlsFieldIndexMapper(val tableName: String) extends IntFieldsByName {
      val byName = {
        val fields = tableNamesToFieldLists(tableName)
        (fields zip fields.indices).toMap
      }
    }

    object MRCONSO extends UmlsFieldIndexMapper("MRCONSO") {
      val CODE = byName("CODE")
      val AUI = byName("AUI")
      val STR = byName("STR")
      val STT = byName("STT")
      val SCUI = byName("SCUI")
      val ISPREF = byName("ISPREF")
      val TTY = byName("TTY")
      val TS = byName("TS")
      val CUI = byName("CUI")
      val LAT = byName("LAT")
      val SAB = byName("SAB")
    }

    object MRREL extends UmlsFieldIndexMapper("MRREL") {
      val AUI1 = byName("AUI1")
      val AUI2 = byName("AUI2")
      val CUI1 = byName("CUI1")
      val CUI2 = byName("CUI2")
      val REL = byName("REL")
      val RELA = byName("RELA")
      val SAB = byName("SAB")
    }

    object MRDEF extends UmlsFieldIndexMapper("MRDEF") {
      val AUI = byName("AUI")
      val DEF = byName("DEF")
      val CUI = byName("CUI")
      val SAB = byName("SAB")
    }

    object MRSAT extends UmlsFieldIndexMapper("MRSAT") {
      val CUI = byName("CUI")
      val CODE = byName("CODE")
      val ATV = byName("ATV")
      val ATN = byName("ATN")
      val SAB = byName("SAB")
    }

    object MRDOC extends UmlsFieldIndexMapper("MRDOC") {
      val VALUE = byName("VALUE")
      val TYPE = byName("TYPE")
      val EXPL = byName("EXPL")
    }

    object MRRANK extends UmlsFieldIndexMapper("MRRANK") {
      val TTY = byName("TTY")
      val RANK = byName("RANK")
      val SAB = byName("SAB")
    }

    object MRSTY extends UmlsFieldIndexMapper("MRSTY") {
      val CUI = byName("CUI")
      val TUI = byName("TUI")
      val STN = byName("STN")
      val STY = byName("STY")
    }

    object MRSAB extends UmlsFieldIndexMapper("MRSAB") {
      val RSAB = byName("RSAB")
    }

    val byName = {
      val indexMappers = Seq(MRCONSO, MRREL, MRDEF, MRSAT, MRDOC, MRRANK, MRSTY, MRSAB)
      indexMappers.map(im => (im.tableName, im)).toMap
    }
  }

  object UmlsFieldIndices {
    trait IntFieldsByName {
      val byName: Map[String, Int]
      def apply(code: String): Option[Int] = byName.get(code)
    }
  }

  class UsageError extends Exception {
    ()
  }

  /**
   * Write from the UMLS RRF data in the source directory to the target directory for all
   *  ontologies listed in <code>ontologyKeys</code>
   *
   *   @param sourceDir The directory containing UMLS RRF data
   *   @param targetDir The directory to which RDF (TTL) files should be written
   *   @param ontologyKeys The UMLS names of the source ontologies of interest
   *
   */
  def writeTriples(sourceDir: File, targetDir: File, ontologyKeys: JIterable[String]): Unit = {
    import scala.collection.JavaConverters._
    writeTriples(sourceDir, targetDir, ontologyKeys.asScala)
  }

  /**
   * Write from the UMLS RRF data in the source directory to the target directory for all
   *  ontologies listed in <code>ontologyKeys</code>
   *
   *   @param sourceDir The directory containing UMLS RRF data
   *   @param targetDir The directory to which RDF (TTL) files should be written
   *   @param ontologyKeys The UMLS names of the source ontologies of interest
   *   @param umlsVersion The version of the UMLS in use (default: 2012AB)
   *
   */
  def writeTriples(sourceDir: File, targetDir: File, ontologyKeys: JIterable[String],
    umlsVersion: String): Unit = {
    import scala.collection.JavaConverters._
    writeTriples(sourceDir, targetDir, ontologyKeys.asScala, umlsVersion)
  }

  /**
   * Write from the UMLS RRF data in the source directory to the target directory for all
   *  ontologies listed in <code>ontologyKeys</code>
   *
   *   @param sourceDir The directory containing UMLS RRF data
   *   @param targetDir The directory to which RDF (TTL) files should be written
   *   @param ontologyKeys The UMLS names of the source ontologies of interest
   *   @param umlsVersion The version of the UMLS in use (default: 2012AB)
   *   @param ontLink If true , link using a skos:isDefinedBy predicate to the
   *     URI of the source ontology
   *   @param cuisAsStrings Encode CUIs as un-prefixed strings instead of URIs
   *   @param storeAtts Read the MRSAT file to store possibly relevant attributes
   *     (time-consuming)
   *   @param cache: Cache entries relevant for other ontologies while reading the files
   */
  def writeTriples(sourceDir: File, targetDir: File, ontologyKeys: JIterable[String],
    umlsVersion: String, ontLink: Boolean, cuisAsStrings: Boolean,
    storeAtts: Boolean, cache: Boolean): Unit = {
    import scala.collection.JavaConverters._
    writeTriples(sourceDir, targetDir, ontologyKeys.asScala, umlsVersion, ontLink,
      cuisAsStrings, storeAtts, cache)
  }

    /**
   * Write from the UMLS RRF data in the source directory to the target directory for all
   *  ontologies listed in <code>ontologyKeys</code>
   *
   *   @param sourceDir The directory containing UMLS RRF data
   *   @param targetDir The directory to which RDF (TTL) files should be written
   *   @param ontologyKeys The UMLS names of the source ontologies of interest
   *   @param umlsVersion The version of the UMLS in use (default: 2012AB)
   *   @param ontLink If true , link using a skos:isDefinedBy predicate to the
   *     URI of the source ontology
   *   @param cuisAsStrings Encode CUIs as un-prefixed strings instead of URIs
   *   @param storeAtts Read the MRSAT file to store possibly relevant attributes
   *     (time-consuming)
   *   @param cache: Cache entries relevant for other ontologies while reading the files
   */
  def writeTriples(sourceDir: File, targetDir: File, ontologyKeys: JIterable[String],
    umlsVersion: String, ontLink: Boolean, cuisAsStrings: Boolean,
    storeAtts: Boolean, cache: Boolean, nThreads: Int): Unit = {
    import scala.collection.JavaConverters._
    writeTriples(sourceDir, targetDir, ontologyKeys.asScala, umlsVersion, ontLink,
      cuisAsStrings, storeAtts, cache, Some(nThreads))
  }
  
  /**
   * Write from the UMLS RRF data in the source directory to the target directory for all
   *  ontologies listed in <code>ontologyKeys</code>
   *
   *   @param sourceDir The directory containing UMLS RRF data
   *   @param targetDir The directory to which RDF (TTL) files should be written
   *   @param ontologyKeys The UMLS names of the source ontologies of interest
   *   @param umlsVersion The version of the UMLS in use (default: 2012AB)
   *   @param ontLink If true link using a skos:isDefinedBy predicate to the
   *     URI of the source ontology
   *   @param cuisAsStrings  (false by default) If true encode CUIs as un-prefixed strings instead of URIs
   *   @param storeAtts  (false by default) Read the MRSAT file to store possibly relevant attributes
   *     (time-consuming)
   *   @param cache  (false by default) Cache entries relevant for other ontologies while reading the files
   *      (EXPERIMENTAL - probably not something you want to use)
   */
  def writeTriples(sourceDir: File, targetDir: File, ontologyKeys: Iterable[String],
      umlsVersion: String = "2012AB", ontLink: Boolean = true, cuisAsStrings: Boolean = false,
      storeAtts: Boolean = false, cache: Boolean = false, nThreads: Option[Int] = None): Unit =
    new RDFConverter(sourceDir).writeTriplesImpl(targetDir, ontologyKeys,
      umlsVersion, ontLink, cuisAsStrings, storeAtts, cache, nThreads)
}

class RDFConverter(dataRoot: File) {
  import RDFConverter._

  val IDXS = new UmlsFieldIndices(dataRoot)

  private val KNOWN_ONT_CODES: Set[String] = {
    val sabTable = new UmlsTable("MRSAB", Seq("RSAB"))
    val codes = sabTable.scan() map { _(0) }
    codes.toSet
  }


  // NOTE: See UmlsOntology.terms() for the reason these functions use -1 and -2
  // indices to obtain the source and target codes, respectively.
  def getRelCodeSource(rel: Seq[String], onCuis: Boolean): String = {
    if (!onCuis) rel(rel.size - 1) else rel(IDXS.MRREL.CUI2)
  }

  def getRelCodeTarget(rel: Seq[String], onCuis: Boolean): String = {
    if (!onCuis) rel(rel.size - 2) else rel(IDXS.MRREL.CUI1)
  }

  def getCode(reg: Seq[String], loadOnCuis: Boolean): String = {
    if (loadOnCuis)
      return reg(IDXS.MRCONSO.CUI)
    if (!reg(IDXS.MRCONSO.CODE).isEmpty())
      return reg(IDXS.MRCONSO.CODE)
    throw new RuntimeException("No code on reg [%s]".format(reg.mkString("|")))
  }

  /***
   * Generate RDF data of UMLS semantic types from stored Metathesaurus data files in
   *  RRF format. This is most likely the method you want to call
   *
   *  @param fileout The path where the UMLS Methesaurus data extracted as RRF is stored
   */
  def generateSemanticTypes(fileout: File): Unit = {
    import scala.collection.mutable.StringBuilder

    val hierarchy = MMap[String, ArrayBuffer[String]]()
    val allNodes = ArrayBuffer[Seq[String]]()
    val mrsty = new UmlsTable("MRSTY", columns = List("TUI", "STN", "STY"))
    val ontTtl = new StringBuilder()
    ontTtl ++= PREFIXES
    val typesSeen = MSet[Seq[String]]()
    for (stt <- mrsty.scan() if !(typesSeen contains stt)) {
      typesSeen += stt
      hierarchy.getOrElseUpdate(stt(1), ArrayBuffer()) += stt(0)
      val styTerm = """
        |umlssty:%s a owl:Class ;
        |	skos:notation "%s"^^xsd:string ;
        |	skos:prefLabel "%s"@en .
        |""".stripMargin.format(stt(0), stt(0), stt(2))
      ontTtl ++= styTerm
      allNodes += stt
    }
    ontTtl ++= "\n"

    for (node <- allNodes) {
      val parent = node(1).replaceFirst("""(\.[^.]+$)|((?<=^[A-Z])\d+$)""", "")
      if (parent != node(1)) {
        for (par <- hierarchy(parent))
          ontTtl ++= "umlssty:%s rdfs:subClassOf umlssty:%s .\n".format(node(0), par)
      }
    }
    ontTtl ++= "\n"
    val fout = new BufferedWriter(new FileWriter(fileout))
    fout.write(ontTtl.result)
    fout.close()
  }

  class UmlsTable(tableName: String, columns: Seq[String] = null,
      cacheableSabs: Set[String] = Set()) {
    val Indexes = IDXS.byName(tableName)
    private var cachedRows: Option[ArrayBuffer[Seq[String]]] = None

    private def colIdx(colname: String): Option[Int] = Indexes(colname)

    def scan(filt: Map[String, String] = null): Iterator[Seq[String]] = {
      def filteredRows() = getRows filter { row =>
        !(filt exists { case (col, tgt) => row(colIdx(col).get) != tgt })
      }
      val rows = if (filt != null) filteredRows else getRows
      rows map { row =>
        if (columns != null)
          columns map { cn => row(colIdx(cn).get) }
        else
          row
      }
    }

    private def getRows(): Iterator[Seq[String]] = {
      if (cachedRows.isDefined) {
        cachedRows.get.toIterator
      } else {
        val caching = cacheableSabs.size > 0
        if (caching)
          cachedRows = Some(ArrayBuffer[Seq[String]]())
        try {
        val src = getRRFSource(dataRoot, tableName)
          src.getLines map { line =>
            val elems = line.split('|').toSeq
            val sab = colIdx("SAB") map { elems(_) } // Some[String] or None
            // cache if table has no SABs, or it does and they're cacheable
            val tableHasSabs = sab.isDefined
            if (caching && (!tableHasSabs || cacheableSabs.contains(sab.get)))
              cachedRows.get += elems
            elems
          }
        } catch {
          case e: FileNotFoundException => throw new MissingUMLSDataException(e)
        } finally {
//          src.close()
        }
      }
    }

  }

  object UmlsTable {
    val _CACHED_TABLES = MMap[Tuple4[String, File, Seq[String], Set[String]], UmlsTable]()

    def get(tableName: String, columns: Seq[String] = null,
      cacheableSabs: Set[String] = Set()): UmlsTable = {
      if (cacheableSabs == None)
        return new UmlsTable(tableName, columns)
      val key = (tableName, dataRoot, columns, cacheableSabs)
      if (!_CACHED_TABLES.contains(key))
        _CACHED_TABLES(key) = new UmlsTable(tableName, columns, cacheableSabs)
      return _CACHED_TABLES(key)
    }

    def clearCache() = _CACHED_TABLES.clear()
  }

  object UmlsClass {

    val STY_URL = "umlssty:"
    val HAS_STY = "umls:hasSTY"
    val HAS_AUI = "umls:aui"
    val HAS_CUI = "umls:cui"
    val HAS_TUI = "umls:tui"

  }

  class UmlsClass(ns: String, atoms: Seq[Seq[String]], relsWithCodes: Seq[RelWithCodes], defs: Seq[Seq[String]],
      atts: Seq[Seq[String]], rank: Seq[Seq[String]], rankByTty: collection.Map[String, collection.Seq[Int]],
      sty: Seq[Seq[String]], styByCui: collection.Map[String, collection.Seq[Int]], loadOnCuis: Boolean = false,
      isRoot: Boolean = false, ontLink: Boolean = true, cuisAsStrings: Boolean = true) {

    import UmlsClass._

    def code(): String = {
      val codes = atoms map { x => getCode(x, loadOnCuis) }
      if (codes.toSet.size != 1)
        LOG.warn("Invalid number of codes (found {}; should be one) for {}",
          codes.mkString(","), atoms.mkString(","), "")
      codes(0)
    }

    private def allLabels(): Seq[String] = atoms map { _(IDXS.MRCONSO.STR) }

    def getAltLabels(prefLabel: String): Set[String] = {
      //isPrefAtoms =  filter(lambda x: x[IDXS.MRCONSO.ISPREF] == 'Y', self.atoms)
      allLabels.filter(_ != prefLabel).toSet
    }

    def getPrefLabel(): String = {
      if (loadOnCuis) {
        if (atoms.length == 1)
          return atoms(0)(IDXS.MRCONSO.STR)

        val labels = allLabels
        if (labels.length == 1)
          return labels(0)

        //if there's only one ISPREF=Y then that one.
        var isPrefAtoms = atoms.filter(x => x(IDXS.MRCONSO.ISPREF) == 'Y')
        if (isPrefAtoms.length == 1)
          return isPrefAtoms(0)(IDXS.MRCONSO.STR)
        else if (isPrefAtoms.length > 1) {
          isPrefAtoms = isPrefAtoms.filter(x => x(IDXS.MRCONSO.STT) == "PF")
          if (isPrefAtoms.length > 0)
            return isPrefAtoms(0)(IDXS.MRCONSO.STR)
        }
        isPrefAtoms = atoms.filter(x => x(IDXS.MRCONSO.STT) == "PF")
        if (isPrefAtoms.length == 1)
          return isPrefAtoms(0)(IDXS.MRCONSO.STR)
        return atoms(0)(IDXS.MRCONSO.STR)
      } else {
        //if ISPREF=Y is not 1 then we look into MRRANK.
        if (rank.length > 0) {
          def sortKey(x: Seq[String]): Integer =
            rank(rankByTty(x(IDXS.MRCONSO.TTY))(0))(IDXS.MRRANK.RANK).toInt
          val mmrankSortedAtoms = atoms.sortBy(sortKey).reverse
          return mmrankSortedAtoms(0)(IDXS.MRCONSO.STR)
        } //there is no rank to use
        else {
          val prefAtom = atoms.filter(x => x(IDXS.MRCONSO.TTY) contains "P")
          if (prefAtom.length == 1)
            return prefAtom(0)(IDXS.MRCONSO.STR)
        }
        throw new RuntimeException("Unable to select pref label")
      }
    }

    def getURLTerm(code: String): String = getUrlTerm(ns, code)

    def toRDF(fmt: String = "Turtle", hierarchy: Boolean = true): String = {
      if (fmt != "Turtle")
        throw new RuntimeException("Only fmt='Turtle' is currently supported")
      val termCode = code()
      val urlTerm = getURLTerm(termCode)
      val prefLabel = getPrefLabel()
      val altLabels = getAltLabels(prefLabel)
      val rdfTerm = new StringBuilder()
      rdfTerm ++= "%s a owl:Class ;\n".format(urlTerm)
      rdfTerm ++= "\tskos:prefLabel \"\"\"%s\"\"\"@en ;\n".format(escape(prefLabel));
      rdfTerm ++= "\tskos:notation \"\"\"%s\"\"\"^^xsd:string ;\n".format(escape(termCode));
      if (altLabels.size > 0) {
        val alVals = altLabels map { al =>
          "\"\"\"%s\"\"\"@en".format(escape(al))
        }
        rdfTerm ++= "\tskos:altLabel %s ;\n".format(alVals.mkString(" , "))
      }
      if (isRoot) {
        rdfTerm ++= "\tumls:isRoot \"true\"^^xsd:boolean ;\n"
        // TODO: Discuss adding this subclass relation.
        //rdfTerm ++= '\trdfs:subClassOf owl:Thing ;\n'
      }

      if (defs.size > 0) {
        val uniqueDefs = defs.map(_(IDXS.MRDEF.DEF)).toSet
        val defVals = uniqueDefs map { d =>
          "\"\"\"%s\"\"\"@en".format(escape(d))
        }
        rdfTerm ++= "\tskos:definition %s ;\n".format(defVals.mkString(" , "))
      }

      for (relWC <- relsWithCodes) {
        val rel = relWC.rel
        if (relWC.sourceCode != termCode)
          throw new RuntimeException("Inconsistent code in rel")
        // Map child relations to rdf:subClassOf (skip parent relations).
        if (rel.reln == "CHD" && hierarchy) {
          rdfTerm ++= "\trdfs:subClassOf %s ;\n".format(getURLTerm(relWC.targetCode))
        } else {
          val p = getURLTerm(rel.relFragment)
          val o = getURLTerm(relWC.targetCode)
          rdfTerm ++= "\t%s %s ;\n".format(p, o)
        }
      }

      for (att <- atts) {
        val atn = att(IDXS.MRSAT.ATN)
        // Skip all these values (they are replicated in MRREL for
        // SNOMEDCT, unknown relationship for MSH).
        //if DEBUG:
        //  LOG.debug("att: %s\n" % str(att))
        //  sys.stderr.flush()
        if (atn != "AQ") {
          val atv = att(IDXS.MRSAT.ATV)
          rdfTerm ++= "\t%s \"\"\"%s\"\"\"^^xsd:string ;\n".format(getURLTerm(atn), escape(atv))
        }
      }
      if (ontLink)
        rdfTerm ++= "\trdfs:isDefinedBy <%s> ;\n".format(ns)

      //auis = set([x[IDXS.MRCONSO.AUI] for x in self.atoms])
      val cuis = atoms.map(_(IDXS.MRCONSO.CUI)).toSet
      val styRecs = cuis map { styByCui } reduce { _ ++ _ }
      val types = styRecs map { idx => sty(idx)(IDXS.MRSTY.TUI) }

      //for t in auis:
      //    rdfTerm += """\t%s \"\"\"%s\"\"\"^^xsd:string ;\n"""%(HAS_AUI, t)
      for (t <- cuis) {
        if (cuisAsStrings)
          rdfTerm ++= "\t%s \"\"\"%s\"\"\"^^xsd:string ;\n".format(HAS_CUI, t)
        else
          rdfTerm ++= "\tskos:closeMatch <umls/%s>;\n".format(t)
      }

      for (t: String <- types.toSet) {
        rdfTerm ++= "\t%s \"\"\"%s\"\"\"^^xsd:string ;\n".format(HAS_TUI, t)
        rdfTerm ++= "\t%s %s ;\n".format(HAS_STY, STY_URL + t)
      }
      rdfTerm ++= "\t.\n\n"
      rdfTerm.result
    }
  }

  class UmlsAttribute(ns: String, att: Seq[String]) {

    def getURLTerm(code: String): String =
      getUrlTerm(ns, code)

    def toRDF(fmt: String = "Turtle"): String = {
      if (fmt != "Turtle")
        throw new RuntimeException("Only fmt='Turtle' is currently supported")
      val rdfTerm = new StringBuilder()
      rdfTerm ++= "<%s> a owl:DatatypeProperty ;".format(getURLTerm(att(IDXS.MRDOC.VALUE)))
      rdfTerm ++= "\trdfs:label \"\"\"%s\"\"\" ;".format(escape(att(IDXS.MRDOC.VALUE)))
      rdfTerm ++= "\trdfs:comment \"\"\"%s\"\"\" .".format(escape(att(IDXS.MRDOC.EXPL)))
      rdfTerm.result
    }

  }

  class UmlsOntology(ontCode: String, umlsVersion: String = "2012AB",
      shouldLoadOnCuis: Option[Boolean] = None, storeAtts: Boolean = true, cacheableOntCodes: Set[String] = Set(),
      ontLink: Boolean = true, cuisAsStrings: Boolean = false) {
    import collection.mutable.HashMap

    trait MapBufferStore[K, V] extends MMap[K, ArrayBuffer[V]] {
      override def apply(key: K) = getOrElseUpdate(key: K, ArrayBuffer[V]())
    }

    class StringSeqStore extends HashMap[String, ArrayBuffer[Int]] with MapBufferStore[String, Int] {}

    private def getStringSeqStore() = new StringSeqStore

    //self.altUriCode = altUriCode
    val atoms = ArrayBuffer[Seq[String]]()
    val atomsByCode = getStringSeqStore
    val atomsByAui = getStringSeqStore
    val rels = ArrayBuffer[Rel]()
    val relsByAuiSrc = getStringSeqStore
    val defs = ArrayBuffer[Seq[String]]()
    val defsByAui = getStringSeqStore
    val atts = ArrayBuffer[Seq[String]]()
    val attsByCode = getStringSeqStore
    val rank = ArrayBuffer[Seq[String]]()
    val rankByTty = getStringSeqStore
    val sty = ArrayBuffer[Seq[String]]()
    val styByCui = getStringSeqStore
    val cuiRoots = MSet[String]()
    var loaded = false
    val namespace = getUmlsUrl(ontCode)

    val loadOnCuis =
      if (shouldLoadOnCuis.isDefined)
        shouldLoadOnCuis.get
      else
        ontCode.startsWith("HL7")

    def loadTables() = {
      val mrconso = UmlsTable.get("MRCONSO", cacheableSabs = cacheableOntCodes)
      var mrconsoFilt = Map("SAB" -> ontCode, "LAT" -> "ENG")
      val relevCuis = MSet[String]()
      for (atom <- mrconso.scan(filt = mrconsoFilt)) {
        var index = atoms.length
        atomsByCode(getCode(atom, loadOnCuis)).append(index)
        if (!loadOnCuis)
          atomsByAui(atom(IDXS.MRCONSO.AUI)).append(index)
        atoms.append(atom)
        relevCuis.add(atom(IDXS.MRCONSO.CUI))
      }
      LOG.debug("length atoms: {}", atoms.length)
      LOG.debug("length atomsByAui: {}", atomsByAui.size)
      LOG.debug("atom example: {}", atoms(0))

      mrconsoFilt = Map("SAB" -> "SRC", "CODE" -> "V-%s".format(ontCode))
      for (atom <- mrconso.scan(filt = mrconsoFilt))
        cuiRoots.add(atom(IDXS.MRCONSO.CUI))
      LOG.debug("length cuiRoots: {}", cuiRoots.size)

      val mrrel = UmlsTable.get("MRREL", cacheableSabs = cacheableOntCodes)
      val genFilt = Map("SAB" -> ontCode)
      val ident1Field = if (loadOnCuis) IDXS.MRREL.CUI1 else IDXS.MRREL.AUI1
      val ident2Field = if (loadOnCuis) IDXS.MRREL.CUI2 else IDXS.MRREL.AUI2
      LOG.trace("Reading table MRREL with filter {}", genFilt)
      for (relRow <- mrrel.scan(filt = genFilt) if relRow(IDXS.MRREL.REL) != "PAR") {
        val ident1 = relRow(ident1Field)
        val ident2 = relRow(ident2Field)
        val index = rels.length
        relsByAuiSrc(ident2).append(index)
        val rel = new Rel(ident1, ident2, relRow(IDXS.MRREL.REL), relRow(IDXS.MRREL.RELA),
            relRow(IDXS.MRREL.CUI1), relRow(IDXS.MRREL.CUI2))
        rels.append(rel)
        if (rels.length % 10000 == 0)
          LOG.trace("Have now read {} rels", rels.length)
      }
      LOG.debug("length rels: {}", rels.length)

      val mrdef = UmlsTable.get("MRDEF", cacheableSabs = cacheableOntCodes)
      val mrdefField = if (loadOnCuis) IDXS.MRDEF.CUI else IDXS.MRDEF.AUI
      for (defi <- mrdef.scan(filt = genFilt)) {
        val index = defs.length
        defsByAui(defi(mrdefField)).append(index)
        defs.append(defi)
      }
      LOG.debug("length defs: {}", defs.length)

      if (storeAtts) {
        val mrsat = UmlsTable.get("MRSAT", cacheableSabs = cacheableOntCodes)
        val field = if (loadOnCuis) IDXS.MRSAT.CUI else IDXS.MRSAT.CODE
        for (att <- mrsat.scan(filt = genFilt) if att(field) != null) {
          val index = atts.length
          attsByCode(att(field)).append(index)
          atts.append(att)
        }
        LOG.debug("length atts: {}", atts.length)
      }

      val mrrank = UmlsTable.get("MRRANK", cacheableSabs = cacheableOntCodes)
      for (rankVals <- mrrank.scan(filt = genFilt)) {
        val index = rankVals.length
        rankByTty(rankVals(IDXS.MRRANK.TTY)) += index
        rank += rankVals
      }
      LOG.debug("length rank: {}", rank.length)

      val mrsty = new UmlsTable("MRSTY")
      for (styVals <- mrsty.scan(filt = null)) {
        val cui = styVals(IDXS.MRSTY.CUI)
        if (relevCuis contains cui) {
          val index = sty.length
          styByCui(cui).append(index)
          sty.append(styVals)
        }
      }
      LOG.debug("length sty: {}", sty.length)
      // Track the loaded status
      loaded = true
      LOG.info("{} tables loaded ...", ontCode)
    }

    def terms(): Iterable[UmlsClass] = {
      if (!loaded)
        loadTables()
      // Note: most UMLS ontologies are 'loadOnCodes' (only HL7 is loadOnCuis)
      atomsByCode map { case (code, rows) =>
        val codeAtoms = rows map { atoms(_) }
        val field = if (loadOnCuis) IDXS.MRCONSO.CUI else IDXS.MRCONSO.AUI
        val ids = codeAtoms map { _(field) }
        val relevRels = ids flatMap { id => relsByAuiSrc(id) map { rels(_) } }
        var relsToClass = ArrayBuffer[RelWithCodes]()
        val isRoot = relevRels exists { cuiRoots contains _.cui1 } 
        if (loadOnCuis) {
          for (rel <- relevRels) 
            relsToClass.append(new RelWithCodes(rel, rel.cui2, rel.cui1))
        } else {
          for (rel <- relevRels) {
            val auiSource = rel.ident2
            val auiTarget = rel.ident1
            val codeSource = atomsByAui(auiSource) map { x => getCode(atoms(x), loadOnCuis) }
            val codeTarget = atomsByAui(auiTarget) map { x => getCode(atoms(x), loadOnCuis) }
            LOG.trace("For {} -> {}, found {} source codes and {} target codes",
              auiSource, auiTarget, codeSource, codeTarget)
            // START HERE - getting no codes for A0974962
            if (codeSource.length != 1 || codeTarget.length > 1)
              LOG.warn("REL {} has an invalid number of source ({}) or target ({}) codes",
                rel, codeSource.mkString(","), codeTarget.mkString(","))
            // throw new RuntimeException("more than one or none codes")
            if (codeSource.length == 1 && codeTarget.length == 1 && codeSource(0) != codeTarget(0)) {
              relsToClass.append(new RelWithCodes(rel, codeSource(0), codeTarget(0)))
            }
          }
        }
        val defs = ids flatMap { id => defsByAui(id).map(this.defs(_)) }
        val atts = attsByCode(code) map { this.atts(_) }

        new UmlsClass(namespace, atoms = codeAtoms, relsWithCodes = relsToClass,
          defs = defs, atts = atts, rank = rank, rankByTty = rankByTty,
          sty = sty, styByCui = styByCui,
          loadOnCuis = loadOnCuis, isRoot = isRoot,
          ontLink = ontLink, cuisAsStrings = cuisAsStrings)
      }
    }

    private[RDFConverter] def writeInto(directory: File, stem: String) = {
      LOG.info("Writing values for %s".format(ontCode))
      loadTables()
      val filePath = new File(directory, s"$stem.ttl")
      def getWriter: BufferedWriter = {
        LOG.debug("Writing values for %s to %s".format(ontCode, filePath))
        new BufferedWriter(new FileWriter(filePath)) // if single, open for append instead
      }
      for (fout <- managed(getWriter)) {
        fout.write(PREFIXES)
        val comment = s"RDF Version of the UMLS ontology $ontCode; " +
          "converted with Metagraph (https://bitbucket.org/nicta_biomed/metagraph)."
        fout.write(ONTOLOGY_HEADER.format(ontCode, comment, umlsVersion, namespace))
        for (term <- terms) 
          fout.write(term.toRDF())
      }
    }
  }

  private def writeTriplesImpl(targetDir: File, ontologyKeys: Iterable[String],
    umlsVersion: String = "2012AB", ontLink: Boolean = true, cuisAsStrings: Boolean = false,
    storeAtts: Boolean = false, cache: Boolean = true, nThreads: Option[Int] = None): Unit = {
    generateSemanticTypes(new File(targetDir, "umls-semantic-types.ttl"))
    val ontKeysSeq = ontologyKeys.toSeq
    for (k <- ontKeysSeq) {
      if (!(KNOWN_ONT_CODES contains k))
        throw new MetagraphConfigException("Unknown ontology key '%s'".format(k))
    }

    //    if (options.cache && options.sources.length > 1)
    val cacheableOntCodes = if (cache && ontologyKeys.size > 1) ontologyKeys.toSet else Set[String]()
    val actualOntKeys = if (cache || nThreads == Option(1)) {
      ontKeysSeq
    } else {
      val parOntKeys = ontKeysSeq.par
      if (nThreads.isDefined)
        parOntKeys.tasksupport = new ForkJoinTaskSupport(new ForkJoinPool(nThreads.get))
      parOntKeys
    }
    for (ontKey <- actualOntKeys) {
      LOG.info("Generating {} (storeAtts={}, ontLink={})", ontKey,
        new java.lang.Boolean(storeAtts), new java.lang.Boolean(ontLink))
      val ont = new UmlsOntology(ontKey, umlsVersion = umlsVersion,
        storeAtts = storeAtts, ontLink = ontLink,
        cacheableOntCodes = cacheableOntCodes, cuisAsStrings = cuisAsStrings)
      ont.writeInto(targetDir, ontKey)
      LOG.info("Ontology {} is done.", ontKey)
    }
    LOG.info("generated MRDOC at global/UMLS level")
  }
}


class Rel(val ident1: String, val ident2: String, val reln: String, val rela: String, val cui1: String, val cui2: String) {
  def relFragment(): String = if (rela != "") rela else reln
  
}

class RelWithCodes(val rel: Rel, val sourceCode: String, val targetCode: String)