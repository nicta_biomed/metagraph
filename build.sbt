import com.typesafe.sbt.SbtStartScript

import SbtStartScript.StartScriptKeys._

import sbtrelease._

import ReleaseStateTransformations._

import ReleaseKeys._

organization := "com.nicta"

name := "metagraph"

scalaVersion := "2.10.3"

seq(SbtStartScript.startScriptForJarSettings: _*)

startScriptName <<= target / "run"

libraryDependencies ++= Seq(
	"org.slf4j" % "slf4j-api" % "1.7.5",
	"net.sourceforge.collections" % "collections-generic" % "4.01",
	"org.openrdf.sesame" % "sesame-rio-turtle" % "2.7.7",
	"org.openrdf.sesame" % "sesame" % "2.7.7",
	"org.openrdf.sesame" % "sesame-query" % "2.7.7",
	"org.openrdf.sesame" % "sesame-queryalgebra-evaluation" % "2.7.7",
	"org.openrdf.sesame" % "sesame-repository-api" % "2.7.7",
	"org.openrdf.sesame" % "sesame-sail-memory" % "2.7.7",
	"org.openrdf.sesame" % "sesame-repository-sail" % "2.7.7",
	"com.beust" % "jcommander" % "1.30",
	"com.typesafe" % "config" % "1.0.1",
	"com.jsuereth" %% "scala-arm" % "1.3"
)

// should be test scope:

libraryDependencies ++= Seq(
	"com.novocode" % "junit-interface" % "0.10" % "test",
	"ch.qos.logback" % "logback-classic" % "1.0.13" % "test"
)

releaseSettings


releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,              // : ReleaseStep
  inquireVersions,                        // : ReleaseStep
  runTest,                                // : ReleaseStep
  setReleaseVersion,                      // : ReleaseStep
  commitReleaseVersion,                   // : ReleaseStep, performs the initial git checks
  tagRelease,                             // : ReleaseStep
  publishArtifacts.copy(action = publishSignedAction),
  setNextVersion,                         // : ReleaseStep
  commitNextVersion,                      // : ReleaseStep
  pushChanges                             // : ReleaseStep, also checks that an upstream branch is properly configured
)

pomIncludeRepository := { _ => false }

pomExtra := (
  <url>http://bitbucket.org/nicta_biomed/metagraph</url>
  <licenses>
    <license>
      <name>BSD 3 Clause</name>
      <url>http://opensource.org/licenses/BSD-3-Clause</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:hg:https://bitbucket.org/nicta_biomed/metagraph</connection>
    <developerConnection>scm:hg:ssh://hg@bitbucket.org/nicta_biomed/metagraph</developerConnection>
    <url>https://bitbucket.org/nicta_biomed/metagraph</url>
  </scm>
  <issueManagement>
    <system>bitbucket</system>
    <url>https://bitbucket.org/nicta_biomed/metagraph/issues</url>
  </issueManagement>
  <developers>
    <developer>
      <id>admackin</id>
      <name>Andy MacKinlay</name>
      <url>http://nicta.com.au/business/health/biomedical_informatics</url>
    </developer>
  </developers>
)


publishMavenStyle := true

publishTo <<= version { v: String =>
  val nexus = "https://oss.sonatype.org/"
  if (v.trim.endsWith("SNAPSHOT")) Some("snapshots" at nexus + "content/repositories/snapshots")
  else Some("staging" at nexus + "service/local/staging/deploy/maven2")
}

publishArtifact in Test := false

// parallelising tests causes race conditions and non-determinism because
// of the way the tests are written here. Disable it:

parallelExecution in Test := false

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")