import sbt._
import Keys._
import com.typesafe.sbt.pgp.PgpKeys

object ProjBuild extends Build {
  lazy val publishSignedAction = { st: State =>
    val extracted = Project.extract(st)
    val ref = extracted.get(thisProjectRef)
    extracted.runAggregated(PgpKeys.publishSigned in Global in ref, st)
  }

  lazy val root = Project(id = "metagraph",
    base = file("."),
    settings = Project.defaultSettings)
}